package it.unibo.cs.snsv.ronf.server;

import org.junit.Test;

import static org.junit.Assert.*;

import org.dbunit.dataset.IDataSet;

import it.unibo.cs.snsv.ronf.client.FrontOffice;

import it.unibo.cs.snsv.ronf.db.Agenzia;
import it.unibo.cs.snsv.ronf.db.AgenziaDTO;

import it.unibo.cs.snsv.ronf.db.AutoDTO;
import it.unibo.cs.snsv.ronf.db.AutoDTO.Tipo.*;

import it.unibo.cs.snsv.ronf.db.ClienteDTO;

import it.unibo.cs.snsv.ronf.db.NoleggioDTO;
import it.unibo.cs.snsv.ronf.db.PagamentoDTO;

import java.util.List;
import java.util.Calendar;

public class FrontOfficeTest extends RONFServerTestCase {
	@Override
	protected IDataSet getDataSet() throws Exception {
		return RONFServerTestCase.dataSet(
			this.getClass(), "FrontOfficeTest.xml"
		);
	}

	@Test
	public void testRegistraCliente() {
		FrontOffice frontoffice = new FrontOfficeImpl();

		ClienteDTO cliente = new ClienteDTO();
		cliente.setNome("Mario");
		cliente.setCognome("Rossi");
		cliente.setPatente("4");

		cliente = frontoffice.registraCliente(cliente);

		assertEquals("4", cliente.getPatente());
		assertEquals("Mario", cliente.getNome());
		assertEquals("Rossi", cliente.getCognome());
	}

	@Test
	public void testCercaCliente() {
		FrontOffice frontoffice = new FrontOfficeImpl();

		ClienteDTO cliente = frontoffice.cercaCliente("3");
		assertEquals("3", cliente.getPatente());
		assertEquals("Pino", cliente.getNome());
		assertEquals("Brutto", cliente.getCognome());
	}

	@Test
	public void testRegistraAuto() {
		FrontOffice frontoffice = new FrontOfficeImpl();

		AutoDTO auto = new AutoDTO();
		auto.setTarga("8");
		auto.setTipo(AutoDTO.Tipo.PRESTIGE);

		auto = frontoffice.registraAuto(auto);

		assertEquals("8", auto.getTarga());
		assertEquals(AutoDTO.Tipo.PRESTIGE, auto.getTipo());

		assertNotNull(auto.getAgenzia());
		assertEquals(3, auto.getAgenzia().getId());
	}

	@Test
	public void testCercaAuto() {
		FrontOffice frontoffice = new FrontOfficeImpl();

		List<AutoDTO> auto =
			frontoffice.cercaAutoPerTipo(AutoDTO.Tipo.MINI);

		assertEquals(2, auto.size());

		assertNotNull(auto.get(0).getAgenzia());
		assertEquals(3, auto.get(0).getAgenzia().getId());
	}

	// @Test
	// public void testCercaAutoRemote() {
	// 	FrontOffice frontoffice = new FrontOfficeImpl();

	// 	List auto = frontoffice.cercaAutoPerTipo(AutoDTO.Tipo.SPORT);

	// 	assertEquals(0, auto.size());
	// }

	@Test
	public void testRegistraNoleggio() {
		AgenziaDTO agenzia = Agenzia.trovaLocale().toDTO();
		Calendar cal = Calendar.getInstance();

		FrontOffice frontoffice = new FrontOfficeImpl();
		NoleggioDTO noleggio = new NoleggioDTO();

		AutoDTO auto = frontoffice.cercaAuto("1");
		ClienteDTO cliente = frontoffice.cercaCliente("1");

		noleggio.setAuto(auto);
		noleggio.setCliente(cliente);
		noleggio.setInizio(cal.getTime());
		noleggio.setDurata(5);
		noleggio.setAgenziaPartenza(agenzia);
		noleggio.setAgenziaArrivo(agenzia);
		noleggio.setNavigatore(true);

		noleggio = frontoffice.registraNoleggio(noleggio);
		assertEquals(2, noleggio.getId());
	}

	@Test
	public void testRegistraPagamento() {
		FrontOffice frontoffice = new FrontOfficeImpl();

		AutoDTO auto = frontoffice.cercaAuto("6");
		assertEquals(false, auto.getDisponibile());

		PagamentoDTO pagamento = frontoffice.calcolaPagamento("6");
		frontoffice.registraPagamento(pagamento, false);

		auto = frontoffice.cercaAuto("6");
		assertEquals(true, auto.getDisponibile());
	}
}
