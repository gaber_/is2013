package it.unibo.cs.snsv.ronf.server;

import org.junit.Test;

import static org.junit.Assert.*;

import org.dbunit.dataset.IDataSet;

import it.unibo.cs.snsv.ronf.client.Manutenzione;

import it.unibo.cs.snsv.ronf.db.Impiegato;
import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO;

import it.unibo.cs.snsv.ronf.db.RifornimentoDTO;
import it.unibo.cs.snsv.ronf.db.TrasferimentoDTO;

import it.unibo.cs.snsv.ronf.db.Rifornimento;
import it.unibo.cs.snsv.ronf.db.Trasferimento;

import java.util.List;

public class ManutenzioneTest extends RONFServerTestCase {
	@Override
	protected IDataSet getDataSet() throws Exception {
		return RONFServerTestCase.dataSet(
			this.getClass(), "ManutenzioneTest.xml"
		);
	}

	@Test
	public void testListaRifornimenti() {
		Manutenzione manutenzione = new ManutenzioneImpl();
		ImpiegatoDTO impiegato= Impiegato.trovaPerId(1).toDTO();

		List rifornimenti = manutenzione.richiediRifornimenti(impiegato);
		assertEquals(10, rifornimenti.size());
		assertEquals(6, Rifornimento.trovaDaAssegnare().size());
	}

	@Test
	public void testListaTrasferimenti() {
		Manutenzione manutenzione = new ManutenzioneImpl();
		ImpiegatoDTO impiegato= Impiegato.trovaPerId(1).toDTO();

		List trasferimenti = manutenzione.richiediTrasferimenti(impiegato);
		assertEquals(2,trasferimenti.size());
		assertEquals(1, Trasferimento.trovaDaAssegnare().size());
	}

	@Test
	public void testRifrnimentoEffettuato() {
		Manutenzione manutenzione = new ManutenzioneImpl();
		assertFalse(Rifornimento.trovaPerId(3).getEseguito());

		manutenzione.rifornimentoEffettuato(3);
		assertTrue(Rifornimento.trovaPerId(3).getEseguito());
	}

	@Test
	public void testTrasferimentoEffettuato() {
		Manutenzione manutenzione = new ManutenzioneImpl();
		assertFalse(Trasferimento.trovaPerId(12).getEseguito());

		manutenzione.trasferimentoEffettuato(12);
		assertTrue(Trasferimento.trovaPerId(12).getEseguito());
	}
}
