package it.unibo.cs.snsv.ronf.server;

import org.junit.Test;

import static org.junit.Assert.*;

import org.dbunit.dataset.IDataSet;

import it.unibo.cs.snsv.ronf.client.Login;

import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO;
import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO.Ruolo.*;

public class LoginTest extends RONFServerTestCase {
	@Override
	protected IDataSet getDataSet() throws Exception {
		return RONFServerTestCase.dataSet(
			this.getClass(), "LoginTest.xml"
		);
	}

	@Test
	public void testCreaUtente() {
		Login login = new LoginImpl();

		ImpiegatoDTO impiegato = new ImpiegatoDTO();
		impiegato.setNome("Mario");
		impiegato.setCognome("Rossi");
		impiegato.setRuolo(ImpiegatoDTO.Ruolo.FRONT_OFFICE);

		impiegato = login.creaUtente(impiegato);

		assertEquals(impiegato.getId(), 2);
		assertEquals(impiegato.getNome(), "Mario");
		assertEquals(impiegato.getCognome(), "Rossi");
		assertEquals(impiegato.getRuolo(),
				ImpiegatoDTO.Ruolo.FRONT_OFFICE);
	}

	@Test
	public void testLoginUtente() {
		Login login = new LoginImpl();

		ImpiegatoDTO impiegato = login.loginUtente(1);

		assertEquals(impiegato.getId(), 1);
		assertEquals(impiegato.getNome(), "Gino");
		assertEquals(impiegato.getCognome(), "Pilotino");
		assertEquals(impiegato.getRuolo(),
			ImpiegatoDTO.Ruolo.MANUTENZIONE);
	}
}
