package it.unibo.cs.snsv.ronf.db;

import org.junit.Test;

import static org.junit.Assert.*;

import org.dbunit.dataset.IDataSet;

import it.unibo.cs.snsv.ronf.db.Impiegato;
import it.unibo.cs.snsv.ronf.db.Impiegato.Ruolo.*;

public class ImpiegatoTest extends RONFDbTestCase {
	@Override
	protected IDataSet getDataSet() throws Exception {
		return RONFDbTestCase.dataSet(
			this.getClass(), "ImpiegatoTest.xml"
		);
	}

	@Test
	public void testTrova() {
		Impiegato impiegato = Impiegato.trovaPerId(1);
		assertEquals(1, impiegato.getId());
		assertEquals("Mario", impiegato.getNome());
		assertEquals("Rossi", impiegato.getCognome());
		assertEquals(Impiegato.Ruolo.FRONT_OFFICE, impiegato.getRuolo());

		impiegato = Impiegato.trovaPerId(3);
		assertEquals(3, impiegato.getId());
		assertEquals("Pino", impiegato.getNome());
		assertEquals("Brutto", impiegato.getCognome());
		assertEquals(Impiegato.Ruolo.MANUTENZIONE, impiegato.getRuolo());
	}

	@Test
	public void testNonTrova() {
		Impiegato impiegato = Impiegato.trovaPerId(42);
		assertEquals(null, impiegato);
	}
}
