package it.unibo.cs.snsv.ronf.db;

import org.junit.Test;

import static org.junit.Assert.*;

import org.dbunit.dataset.IDataSet;

import it.unibo.cs.snsv.ronf.db.Agenzia;

import it.unibo.cs.snsv.ronf.db.Auto;
import it.unibo.cs.snsv.ronf.db.Auto.Tipo.*;

import java.util.List;

public class AutoTest extends RONFDbTestCase {
	@Override
	protected IDataSet getDataSet() throws Exception {
		return RONFDbTestCase.dataSet(this.getClass(), "AutoTest.xml");
	}

	@Test
	public void testTrovaPerTarga() {
		Auto auto = Auto.trovaPerTarga("4");
		assertNotNull(auto);

		assertEquals(Auto.Tipo.SPORT, auto.getTipo());
		assertEquals(false, auto.getDisponibile());
		assertEquals("4", auto.getTarga());
	}

	@Test
	public void testCrea() {
		Auto auto = new Auto();

		Agenzia agenzia = Agenzia.trovaLocale();
		assertNotNull(agenzia);

		auto.setTipo(Auto.Tipo.FAMILY);
		auto.setMarca("asd");
		auto.setModello("jhkjn");
		auto.setTarga("7");
		auto.setDisponibile(true);
		auto.setAgenzia(agenzia);

		assertNotNull(auto.getAgenzia());
		assertEquals(3, auto.getAgenzia().getId());

		auto.crea();

		auto = Auto.trovaPerTarga("7");
		assertNotNull(auto);

		assertEquals(Auto.Tipo.FAMILY, auto.getTipo());
		assertEquals("asd", auto.getMarca());
		assertEquals("jhkjn", auto.getModello());
		assertEquals("7", auto.getTarga());
		assertEquals(true, auto.getDisponibile());

		assertNotNull(auto.getAgenzia());
		assertEquals(3, auto.getAgenzia().getId());
	}

	@Test
	public void testTrovaPerTipo() {
		List<Auto> auto = Auto.trovaPerTipo(Auto.Tipo.MINI, true);

		assertEquals(auto.size(), 2);

		for (Auto i : auto) {
			assertEquals(Auto.Tipo.MINI, i.getTipo());
			assertEquals(true, i.getDisponibile());
		}
	}

	@Test
	public void testNonTrova() {
		Auto auto = Auto.trovaPerTarga("42");
		assertNull(auto);
	}
}
