package it.unibo.cs.snsv.ronf.db;

import org.junit.Test;

import static org.junit.Assert.*;

import org.dbunit.dataset.IDataSet;

import it.unibo.cs.snsv.ronf.db.Pagamento;
import it.unibo.cs.snsv.ronf.db.Pagamento.Metodo.*;

import it.unibo.cs.snsv.ronf.db.Auto;
import it.unibo.cs.snsv.ronf.db.Cliente;
import it.unibo.cs.snsv.ronf.db.Noleggio;

import java.util.Date;

public class PagamentoTest extends RONFDbTestCase {
	@Override
	protected IDataSet getDataSet() throws Exception {
		return RONFDbTestCase.dataSet(
			this.getClass(), "PagamentoTest.xml"
		);
	}

	@Test
	public void testCrea() {
		Noleggio noleggio = Noleggio.trovaPerId(1);
		assertNotNull(noleggio);

		Pagamento pagamento = noleggio.calcolaPagamento(true);
		assertEquals((int) 49.0, (int) pagamento.getImporto());

		pagamento.setMetodo(Pagamento.Metodo.CONTANTI);
		pagamento.crea();
	}

	// @Test
	// public void testTrovaPerId() {
	// 	Pagamento pagamento = Pagamento.trovaPerId(1);
	// 	assertNotNull(pagamento);

	// 	assertTrue(pagamento.getImporto() > 0);
	// 	assertEquals(pagamento.getMetodo(), Pagamento.Metodo.CONTANTI);
	// }
}
