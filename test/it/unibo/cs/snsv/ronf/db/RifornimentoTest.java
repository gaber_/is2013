package it.unibo.cs.snsv.ronf.db;

import org.junit.Test;

import static org.junit.Assert.*;

import org.dbunit.dataset.IDataSet;

import it.unibo.cs.snsv.ronf.db.Rifornimento;

import it.unibo.cs.snsv.ronf.db.Auto;
import it.unibo.cs.snsv.ronf.db.Impiegato;
import it.unibo.cs.snsv.ronf.db.Impiegato.Ruolo.*;

import java.util.List;

public class RifornimentoTest extends RONFDbTestCase {
	@Override
	protected IDataSet getDataSet() throws Exception {
		return RONFDbTestCase.dataSet(
			this.getClass(), "RifornimentoTest.xml"
		);
	}

	@Test
	public void testTrovaPerId() {
		Auto auto = Auto.trovaPerTarga("1");
		Rifornimento rif = Rifornimento.trovaPerId(1);

		assertEquals(auto.getTarga(), rif.getAuto().getTarga());
	}

	@Test
	public void testTrovaDaAssegnare() {
		Impiegato mario = Impiegato.trovaPerId(1);
		assertNotNull(mario);

		List<Rifornimento> lista = Rifornimento.trovaDaAssegnare();
		assertNotNull(lista);

		assertEquals(6, lista.size());
	}

	@Test
	public void testAssegnaAddetto() {
		Impiegato mario = Impiegato.trovaPerId(1);
		assertNotNull(mario);

		List<Rifornimento> lista = Rifornimento.trovaDaAssegnare();
		assertNotNull(lista);

		for (Rifornimento i : lista) {
			i.setAddetto(mario);
			i.aggiorna();
		}

		assertEquals(0, Rifornimento.trovaDaAssegnare().size());
		assertEquals(11, Rifornimento.trovaPerAddetto(mario).size());
	}

	@Test
	public void testSegnaEseguito() {
		List<Rifornimento> lista = Rifornimento.trovaDaAssegnare();
		assertNotNull(lista);

		for (Rifornimento i : lista) {
			i.setEseguito(true);
			i.aggiorna();
		}

		assertEquals(0, Rifornimento.trovaDaAssegnare().size());
	}
}
