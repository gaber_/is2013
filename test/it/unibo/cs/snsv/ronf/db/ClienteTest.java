package it.unibo.cs.snsv.ronf.db;

import org.junit.Test;

import static org.junit.Assert.*;

import org.dbunit.dataset.IDataSet;

import it.unibo.cs.snsv.ronf.db.Cliente;

public class ClienteTest extends RONFDbTestCase {
	@Override
	protected IDataSet getDataSet() throws Exception {
		return RONFDbTestCase.dataSet(
			this.getClass(), "ClienteTest.xml"
		);
	}

	@Test
	public void testTrovaPerPatente() {
		Cliente cliente = Cliente.trovaPerPatente("1");
		assertNotNull(cliente);

		assertEquals("Mario" ,cliente.getNome());
		assertEquals("Rossi", cliente.getCognome());
		assertEquals("1", cliente.getPatente());

		cliente = Cliente.trovaPerPatente("3");
		assertNotNull(cliente);

		assertEquals("Pino", cliente.getNome());
		assertEquals("Brutto", cliente.getCognome());
		assertEquals("3", cliente.getPatente());
	}

	@Test
	public void testCrea() {
		Cliente cliente = new Cliente();

		cliente.setNome("Lol");
		cliente.setCognome("Mao");
		cliente.setPatente("5");

		cliente.crea();

		cliente = Cliente.trovaPerPatente("5");
		assertEquals("Lol" ,cliente.getNome());
		assertEquals("Mao", cliente.getCognome());
		assertEquals("5", cliente.getPatente());
	}

	@Test
	public void testNonTrova() {
		Cliente cliente = Cliente.trovaPerPatente("42");
		assertEquals(null, cliente);
	}
}
