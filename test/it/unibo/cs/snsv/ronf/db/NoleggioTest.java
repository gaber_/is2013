package it.unibo.cs.snsv.ronf.db;

import org.junit.Test;

import static org.junit.Assert.*;

import org.dbunit.dataset.IDataSet;

import it.unibo.cs.snsv.ronf.db.Noleggio;

import it.unibo.cs.snsv.ronf.db.Auto;
import it.unibo.cs.snsv.ronf.db.Auto.Tipo.*;
import it.unibo.cs.snsv.ronf.db.Cliente;
import it.unibo.cs.snsv.ronf.db.Noleggio;
import it.unibo.cs.snsv.ronf.db.Pagamento;

import java.util.Date;
import java.util.List;

public class NoleggioTest extends RONFDbTestCase {
	@Override
	protected IDataSet getDataSet() throws Exception {
		return RONFDbTestCase.dataSet(
			this.getClass(), "NoleggioTest.xml"
		);
	}

	@Test
	public void testTrovaPerId() {
		Noleggio noleggio = Noleggio.trovaPerId(1);
		assertEquals(5, noleggio.getDurata());
		assertEquals(false, noleggio.inCorso());
	}

	@Test
	public void testTrovaPerAuto() {
		Auto auto = Auto.trovaPerTipo(Auto.Tipo.SPORT, true).get(0);
		assertNotNull(auto);

		List<Noleggio> noleggio = Noleggio.trovaPerAuto(auto, true);
		assertEquals(1, noleggio.size());

		assertEquals(2, noleggio.get(0).getId());
	}

	@Test
	public void testCalcolaPagamento() throws Exception {
		Auto auto = Auto.trovaPerTipo(Auto.Tipo.SPORT, true).get(0);
		Noleggio noleggio = Noleggio.trovaPerAuto(auto, true).get(0);
		assertNotNull(noleggio);

		Pagamento pagamento = noleggio.calcolaPagamento(true);
		assertEquals(noleggio.getId(), pagamento.getNoleggio().getId());
		assertEquals(2, noleggio.getId());

		noleggio.setAgenziaArrivo(Agenzia.trovaLocale());
		noleggio.aggiorna();

		pagamento.crea();

		List<Noleggio> noleggi = Noleggio.trovaPerAuto(auto, true);
		assertEquals(0, noleggi.size());

		pagamento = Pagamento.trovaPerId(2);
		assertEquals(noleggio.getId(), pagamento.getNoleggio().getId());
		assertNotNull(pagamento);
	}
}
