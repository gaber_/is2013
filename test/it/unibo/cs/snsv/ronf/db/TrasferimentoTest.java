package it.unibo.cs.snsv.ronf.db;

import org.junit.Test;

import static org.junit.Assert.*;

import org.dbunit.dataset.IDataSet;

import it.unibo.cs.snsv.ronf.db.Trasferimento;

import it.unibo.cs.snsv.ronf.db.Auto;
import it.unibo.cs.snsv.ronf.db.Impiegato;
import it.unibo.cs.snsv.ronf.db.Impiegato.Ruolo.*;

import java.util.List;

public class TrasferimentoTest extends RONFDbTestCase {
	@Override
	protected IDataSet getDataSet() throws Exception {
		return RONFDbTestCase.dataSet(
			this.getClass(), "TrasferimentoTest.xml"
		);
	}

	@Test
	public void testTrovaPerId() {
		Auto auto = Auto.trovaPerTarga("1");
		Trasferimento trasf = Trasferimento.trovaPerId(1);

		assertEquals(auto.getTarga(), trasf.getAuto().getTarga());
		assertEquals("Agenzia n. 3",
				trasf.getAgenziaPartenza().getNome());
		assertEquals("Agenzia n. 3",
				trasf.getAgenziaArrivo().getNome());
	}

	@Test
	public void testAssegnaAddetto() {
		Impiegato mario = Impiegato.trovaPerId(1);
		assertNotNull(mario);

		List<Trasferimento> lista = Trasferimento.trovaDaAssegnare();
		assertNotNull(lista);

		for (Trasferimento i : lista) {
			i.setAddetto(mario);
			i.aggiorna();
		}

		assertEquals(0, Trasferimento.trovaDaAssegnare().size());
		assertEquals(6, Trasferimento.trovaPerAddetto(mario).size());
	}

	@Test
	public void testSegnaEseguito() {
		List<Trasferimento> lista = Trasferimento.trovaDaAssegnare();
		assertNotNull(lista);

		for (Trasferimento i : lista) {
			i.setEseguito(true);
			i.aggiorna();
		}

		assertEquals(0, Trasferimento.trovaDaAssegnare().size());
	}
}
