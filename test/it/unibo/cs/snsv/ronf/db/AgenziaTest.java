package it.unibo.cs.snsv.ronf.db;

import org.junit.Test;

import static org.junit.Assert.*;

import org.dbunit.dataset.IDataSet;

import it.unibo.cs.snsv.ronf.db.Agenzia;

import java.util.List;

public class AgenziaTest extends RONFDbTestCase {
	@Override
	protected IDataSet getDataSet() throws Exception {
		return RONFDbTestCase.dataSet(
			this.getClass(), "AgenziaTest.xml"
		);
	}

	@Test
	public void testTrovaPerId() {
		Agenzia agenzia = Agenzia.trovaPerId(3);
		assertNotNull(agenzia);

		assertEquals(3, agenzia.getId());
		assertEquals("Agenzia n. 3", agenzia.getNome());
	}

	@Test
	public void testTrovaLocale() {
		Agenzia agenzia = Agenzia.trovaLocale();
		assertNotNull(agenzia);

		assertEquals(3, agenzia.getId());
		assertEquals("Agenzia n. 3", agenzia.getNome());
	}

	@Test
	public void testTrovaRemote() {
		List<Agenzia> agenzie = Agenzia.trovaRemote();

		Agenzia agenzia = agenzie.get(0);
		assertEquals(2, agenzia.getId());
		assertEquals("Agenzia n. 2", agenzia.getNome());
	}
}
