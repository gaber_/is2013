package it.unibo.cs.snsv.ronf.db;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.dbunit.DBTestCase;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;

import org.dbunit.database.DatabaseConfig;

import org.dbunit.operation.DatabaseOperation;
import org.dbunit.operation.DatabaseOperation.*;

import org.dbunit.ext.hsqldb.HsqldbDataTypeFactory;

import org.dbunit.PropertiesBasedJdbcDatabaseTester;

import org.apache.commons.lang.NotImplementedException;

import it.unibo.cs.snsv.ronf.server.ConfigUtil;

public abstract class RONFDbTestCase extends DBTestCase {
	public RONFDbTestCase() {
		System.setProperty(PropertiesBasedJdbcDatabaseTester
			.DBUNIT_DRIVER_CLASS, "org.hsqldb.jdbcDriver");
		System.setProperty(PropertiesBasedJdbcDatabaseTester
			.DBUNIT_CONNECTION_URL, "jdbc:hsqldb:file:testdb");
		System.setProperty(PropertiesBasedJdbcDatabaseTester
				.DBUNIT_USERNAME, "sa");
		System.setProperty(PropertiesBasedJdbcDatabaseTester
				.DBUNIT_PASSWORD, "");
	}

	@Override
	protected void setUpDatabaseConfig(DatabaseConfig config) {
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
				new HsqldbDataTypeFactory());
	}

	public static IDataSet dataSet(Class cl, String file) throws Exception {
		FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
		builder.setColumnSensing(true);
		return builder.build(cl.getResourceAsStream(file));
	}

	@Before
	public void setUp() throws Exception {
		ConfigUtil.buildConfig();
		HibernateUtil.buildSessionFactory();

		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();

		HibernateUtil.getSessionFactory().close();
	}

	/** {@inheritDoc} */
	protected IDataSet getDataSet() throws Exception {
		throw new NotImplementedException(
				"Specify data set for test: "
				+ this.getClass().getSimpleName());
	}

	/** {@inheritDoc} */
	protected DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.REFRESH;
	}

	/** {@inheritDoc} */
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}
}
