Rental ON the Fly
=================

## BUILD

```bash
$ ant build
```

## TEST

```bash
$ ant test.dev
```

## DEPLOYMENT

* Modificare il file `conf/hibernate.cfg.xml` con le opzioni desiderate per il
  database (es. tipo di database, nome utente, password, ...).
* Modificare il file `conf/RONF.cfg.xml` con le opzioni desiderate per il server
  RONF (es. listino prezzi, agenzie remote, ...).
* Creare il file war con il comando `ant war`
* Muovere il file war nella cartella di deployment
* Alternativamente, eseguire `java -jar libs/jetty-runner.jar RONF.war` per
  avviare il server usando Jetty.
