package it.unibo.cs.snsv.ronf.db;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

import it.unibo.cs.snsv.ronf.db.PagamentoDTO;

import org.dozer.DozerBeanMapperSingletonWrapper;

import it.unibo.cs.snsv.ronf.db.Noleggio;

@Entity
@Table(name = "PAGAMENTI")
public class Pagamento extends Oggetto {
	public enum Metodo {
		CARTA_DI_CREDITO,
		CONTANTI
	}

	@Id
	@GeneratedValue
	@Column
	private long id;

	@Column
	@Enumerated(EnumType.STRING)
	private Metodo metodo;

	@ManyToOne
	@JoinColumn
	private Noleggio noleggio;

	@Column
	private double importo;

	public Pagamento() { }

	public long getId() { return id; }
	public void setId(long id) { this.id = id; }

	public Metodo getMetodo() { return metodo; }
	public void setMetodo(Metodo metodo) { this.metodo = metodo; }

	public Noleggio getNoleggio() { return noleggio; }
	public void setNoleggio(Noleggio noleggio) { this.noleggio = noleggio; }

	public double getImporto() { return importo; }
	public void setImporto(double importo) { this.importo = importo; }

	public static Pagamento trovaPerId(long id) {
		return Pagamento.trovaPerId(Pagamento.class, id);
	}

	public static Pagamento fromDTO(PagamentoDTO dto) {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(dto, Pagamento.class);
	}

	public PagamentoDTO toDTO() {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(this, PagamentoDTO.class);
	}
}
