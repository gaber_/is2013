package it.unibo.cs.snsv.ronf.db;

import org.hibernate.Hibernate;
import org.hibernate.Hibernate.*;
import org.hibernate.HibernateException;
import org.hibernate.type.LongType;
import org.hibernate.usertype.UserType;
import org.hibernate.engine.spi.SessionImplementor;

import it.unibo.cs.snsv.ronf.db.AutoDTO;

import it.unibo.cs.snsv.ronf.server.ConfigUtil;

import org.dozer.DozerBeanMapperSingletonWrapper;

import java.util.List;
import java.util.LinkedList;
import java.io.Serializable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.sql.Types.*;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

import org.apache.commons.lang3.ObjectUtils;

public class Agenzia implements UserType {
	private long id;

	private String nome;

	private String url;

	public Agenzia() { }

	public long getId() { return id; }
	public void setId(long id) { this.id = id; }

	public String getNome() { return nome; }
	public void setNome(String nome) { this.nome = nome; }

	public String getUrl() { return url; }
	public void setUrl(String url) { this.url = url; }

	public static Agenzia trovaPerId(long id) {
		Agenzia agenzia = new Agenzia();

		String expr   = "//ronf-config/agenzie/agenzia[@id='"+id+"']";
		NodeList list = ConfigUtil.getNodeList(expr);

		if (list.getLength() == 0)
			return null;

		NamedNodeMap attrs = list.item(0).getAttributes();

		agenzia.setId(id);
		agenzia.setNome(attrs.getNamedItem("nome").getNodeValue());
		agenzia.setUrl(attrs.getNamedItem("url").getNodeValue());

		return agenzia;
	}

	public static Agenzia trovaLocale() {
		Agenzia agenzia = new Agenzia();

		String expr = "//ronf-config/agenzie/@locale";
		long id = ConfigUtil.getLong(expr);

		expr = "//ronf-config/agenzie/agenzia[@id='" + id + "']";
		NodeList list = ConfigUtil.getNodeList(expr);

		if (list.getLength() == 0)
			return null;

		NamedNodeMap attrs = list.item(0).getAttributes();

		agenzia.setId(id);
		agenzia.setNome(attrs.getNamedItem("nome").getNodeValue());
		agenzia.setUrl(attrs.getNamedItem("url").getNodeValue());

		return agenzia;
	}

	public static List<Agenzia> trovaRemote() {
		List agenzie = new LinkedList();

		Agenzia locale = trovaLocale();

		String expr = "//ronf-config/agenzie/agenzia";
		NodeList list = ConfigUtil.getNodeList(expr);

		for(int i = 0; i < list.getLength(); i++){
			Node node = list.item(i);
			Agenzia agenzia = new Agenzia();

			NamedNodeMap attrs = node.getAttributes();

			long id = Long.parseLong(attrs.getNamedItem("id")
						.getNodeValue());

			if (id == locale.getId())
				continue;

			agenzia.setId(id);
			agenzia.setNome(attrs.getNamedItem("nome")
						.getNodeValue());
			agenzia.setUrl(attrs.getNamedItem("url")
						.getNodeValue());

			agenzie.add(agenzia);
		}

		return agenzie;
	}

	public static Agenzia fromDTO(AgenziaDTO dto) {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(dto, Agenzia.class);
	}

	public AgenziaDTO toDTO() {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(this, AgenziaDTO.class);
	}

	/*
	 * Hibernate stuff
	 */

	public int[] sqlTypes() {
		return new int[] { Types.NUMERIC };
	}

	public Class returnedClass() {
		return Agenzia.class;
	}

	public Object nullSafeGet(ResultSet rs, String[] names,
				  SessionImplementor session, Object owner)
							throws SQLException {
		Long id = (Long) LongType.INSTANCE.get(rs, names[0], session);

		Agenzia agenzia;

		if (id == null)
			return null;

		agenzia = trovaPerId(id);

		return (Object) agenzia;
	}

	public void nullSafeSet(PreparedStatement st, Object value, int index,
				SessionImplementor session)
							throws SQLException {
		long id;
		Agenzia agenzia = (Agenzia) value;

		if (agenzia == null)
			LongType.INSTANCE.set(st, null, index, session);
		else
			LongType.INSTANCE.set(st, agenzia.getId(), index, session);
	}

	@Override
	public boolean isMutable() {
		return false;
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		Agenzia a = (Agenzia) x;
		Agenzia b = (Agenzia) y;

		if (a == null || b == null)
			return false;

		return (a.getId() == b.getId());
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}

	@Override
	public int hashCode(Object x) throws HibernateException {
		assert (x != null);
		return x.hashCode();
	}

	@Override
	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}

	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}

	@Override
	public Object assemble(Serializable cached, Object owner) throws HibernateException {
		return cached;
	}
}
