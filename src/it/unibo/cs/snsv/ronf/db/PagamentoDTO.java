package it.unibo.cs.snsv.ronf.db;

import java.io.Serializable;

import it.unibo.cs.snsv.ronf.db.NoleggioDTO;

public class PagamentoDTO implements Serializable {
	public enum Metodo {
		CARTA_DI_CREDITO,
		CONTANTI
	}

	private long id;

	private Metodo metodo;

	private NoleggioDTO noleggio;

	private double importo;

	public PagamentoDTO() { }

	public long getId() { return id; }
	public void setId(long id) { this.id = id; }

	public Metodo getMetodo() { return metodo; }
	public void setMetodo(Metodo metodo) { this.metodo = metodo; }

	public NoleggioDTO getNoleggio() { return noleggio; }
	public void setNoleggio(NoleggioDTO noleggio) { this.noleggio = noleggio; }

	public double getImporto() { return importo; }
	public void setImporto(double importo) { this.importo = importo; }
}
