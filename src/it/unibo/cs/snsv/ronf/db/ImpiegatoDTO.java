package it.unibo.cs.snsv.ronf.db;

import java.io.Serializable;

public class ImpiegatoDTO implements Serializable {
	public enum Ruolo {
		FRONT_OFFICE,
		MANUTENZIONE
	}

	private long id;

	private String nome;

	private String cognome;

	private Ruolo ruolo;

	public ImpiegatoDTO() { super(); }

	public long getId() { return id; }
	public void setId(long id) { this.id = id; }

	public String getNome() { return nome; }
	public void setNome(String nome) { this.nome = nome; }

	public String getCognome() { return cognome; }
	public void setCognome(String cognome) { this.cognome = cognome; }

	public Ruolo getRuolo() { return ruolo; }
	public void setRuolo(Ruolo ruolo) { this.ruolo = ruolo; }
}
