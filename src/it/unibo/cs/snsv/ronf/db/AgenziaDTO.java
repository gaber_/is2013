package it.unibo.cs.snsv.ronf.db;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class AgenziaDTO implements IsSerializable, Serializable {
	private long id;

	private String nome;

	private String url;

	public AgenziaDTO() { }

	public long getId() { return id; }
	public void setId(long id) { this.id = id; }

	public String getNome() { return nome; }
	public void setNome(String nome) { this.nome = nome; }

	public String getUrl() { return url; }
	public void setUrl(String url) { this.url = url; }
}
