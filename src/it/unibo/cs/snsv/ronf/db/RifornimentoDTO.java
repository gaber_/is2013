package it.unibo.cs.snsv.ronf.db;

import java.io.Serializable;

import it.unibo.cs.snsv.ronf.db.AutoDTO;
import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO;

public class RifornimentoDTO implements Serializable {
	private long id;

	private AutoDTO auto;

	private ImpiegatoDTO addetto = null;

	private boolean eseguito = false;

	public RifornimentoDTO() { }

	public long getId() { return id; }
	public void setId(long id) { this.id = id; }

	public AutoDTO getAuto() { return auto; }
	public void setAuto(AutoDTO auto) { this.auto = auto; }

	public ImpiegatoDTO getAddetto() { return addetto; }
	public void setAddetto(ImpiegatoDTO addetto) { this.addetto = addetto; }

	public boolean getEseguito() { return eseguito; }
	public void setEseguito(boolean eseguito) { this.eseguito = eseguito; }
}
