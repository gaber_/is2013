package it.unibo.cs.snsv.ronf.db;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;

import org.hibernate.Session;
import org.hibernate.Transaction;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import it.unibo.cs.snsv.ronf.db.Agenzia;

import it.unibo.cs.snsv.ronf.db.AutoDTO;

import org.dozer.DozerBeanMapperSingletonWrapper;

import java.util.List;

@Entity
@Table(name = "AUTO")
public class Auto extends Oggetto {
	public enum Tipo {
		MINI,
		FAMILY,
		SPORT,
		PRESTIGE
	}

	@Column
	@Enumerated(EnumType.STRING)
	private Tipo tipo;

	@Id
	@Column
	private String targa;

	@Column
	private String marca;

	@Column
	private String modello;

	@Column
	private boolean disponibile = true;

	@Column(name = "agenzia_id")
	private Agenzia agenzia;

	public Auto() { }

	public Tipo getTipo() { return tipo; }
	public void setTipo(Tipo tipo) { this.tipo = tipo; }

	public String getTarga() { return targa; }
	public void setTarga(String targa) { this.targa = targa; }

	public String getMarca() { return marca; }
	public void setMarca(String marca) { this.marca = marca; }

	public String getModello() { return modello; }
	public void setModello(String modello) { this.modello = modello; }

	public boolean getDisponibile() { return disponibile; }
	public void setDisponibile(boolean disponibile) {
		this.disponibile = disponibile;
	}

	public Agenzia getAgenzia() { return agenzia; }
	public void setAgenzia(Agenzia agenzia) { this.agenzia = agenzia; }

	public static Auto trovaPerTarga(String targa) {
		Auto auto;

		Session session = HibernateUtil
			.getSessionFactory()
			.getCurrentSession();

		session.beginTransaction();
		auto = (Auto) session.createCriteria(Auto.class)
		       .add(Restrictions.eq("targa", targa))
		       .uniqueResult();
		session.getTransaction().commit();

		return auto;
	}

	/**
	 * Trova tutte le auto di un certo tipo.
	 *
	 * @param tipo        il tipo dell'auto
	 * @param disponibile se "true" ritorna solo le auto disponibili
	 * @return            lista di auto
	 */
	public static List<Auto> trovaPerTipo(Tipo tipo, boolean disponibile) {
		List auto;

		Session session = HibernateUtil
			.getSessionFactory()
			.getCurrentSession();

		session.beginTransaction();
		auto = session.createCriteria(Auto.class)
		       .add(Restrictions.eq("tipo", tipo))
		       .add(Restrictions.eq("disponibile", disponibile))
		       .list();
		session.getTransaction().commit();

		return auto;
	}

	public static Auto fromDTO(AutoDTO dto) {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(dto, Auto.class);
	}

	public AutoDTO toDTO() {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(this, AutoDTO.class);
	}
}
