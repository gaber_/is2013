package it.unibo.cs.snsv.ronf.db;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static SessionFactory sessionFactory = buildSessionFactory();

	public static SessionFactory buildSessionFactory() {
		try {
			sessionFactory = new Configuration()
				.addAnnotatedClass(Auto.class)
				.addAnnotatedClass(Cliente.class)
				.addAnnotatedClass(Impiegato.class)
				.addAnnotatedClass(Noleggio.class)
				.addAnnotatedClass(Pagamento.class)
				.addAnnotatedClass(Rifornimento.class)
				.addAnnotatedClass(Trasferimento.class)
				.configure()
				.buildSessionFactory();

			return sessionFactory;
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
