package it.unibo.cs.snsv.ronf.db;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

import org.hibernate.Session;
import org.hibernate.Transaction;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import it.unibo.cs.snsv.ronf.db.Auto;
import it.unibo.cs.snsv.ronf.db.Impiegato;

import it.unibo.cs.snsv.ronf.db.RifornimentoDTO;

import org.dozer.DozerBeanMapperSingletonWrapper;

import java.util.List;

@Entity
@Table(name = "RIFORNIMENTI")
public class Rifornimento extends Oggetto {
	@Id
	@GeneratedValue
	@Column
	private long id;

	@ManyToOne
	@JoinColumn
	private Auto auto;

	@ManyToOne
	@JoinColumn(nullable = true)
	private Impiegato addetto = null;

	@Column
	private boolean eseguito = false;

	public Rifornimento() { }

	public long getId() { return id; }
	public void setId(long id) { this.id = id; }

	public Auto getAuto() { return auto; }
	public void setAuto(Auto auto) { this.auto = auto; }

	public Impiegato getAddetto() { return addetto; }
	public void setAddetto(Impiegato addetto) { this.addetto = addetto; }

	public boolean getEseguito() { return eseguito; }
	public void setEseguito(boolean eseguito) { this.eseguito = eseguito; }

	public static Rifornimento trovaPerId(long id) {
		return Oggetto.trovaPerId(Rifornimento.class, id);
	}

	/**
	 * Trova tutti i rifornimenti non ancora eseguiti e senza addetto.
	 *
	 * @return lista di rifornimenti
	 */
	public static List<Rifornimento> trovaDaAssegnare() {
		List rifornimenti;

		Session session = HibernateUtil
			.getSessionFactory()
			.getCurrentSession();

		session.beginTransaction();
		rifornimenti = session.createCriteria(Rifornimento.class)
		       .add(Restrictions.and(
		         Restrictions.eq("eseguito", false),
		         Restrictions.isNull("addetto")
		       )).list();
		session.getTransaction().commit();

		return rifornimenti;
	}

	/**
	 * Trova i rifornimenti non eseguiti e assegnati a un dato addetto.
	 *
	 * @param addetto l'addetto assegnato
	 * @return        lista di rifornimenti
	 */
	public static List<Rifornimento> trovaPerAddetto(Impiegato addetto) {
		List rifornimenti;

		Session session = HibernateUtil
			.getSessionFactory()
			.getCurrentSession();

		session.beginTransaction();
		rifornimenti = session.createCriteria(Rifornimento.class)
		       .add(Restrictions.and(
		         Restrictions.eq("eseguito", false),
		         Restrictions.eq("addetto", addetto)
		       )).list();
		session.getTransaction().commit();

		return rifornimenti;
	}

	public static Rifornimento fromDTO(RifornimentoDTO dto) {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(dto, Rifornimento.class);
	}

	public RifornimentoDTO toDTO() {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(this, RifornimentoDTO.class);
	}
}
