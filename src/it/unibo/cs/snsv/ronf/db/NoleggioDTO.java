package it.unibo.cs.snsv.ronf.db;

import java.io.Serializable;

import it.unibo.cs.snsv.ronf.db.AgenziaDTO;
import it.unibo.cs.snsv.ronf.db.AutoDTO;
import it.unibo.cs.snsv.ronf.db.AutoDTO.Tipo.*;
import it.unibo.cs.snsv.ronf.db.ClienteDTO;
import it.unibo.cs.snsv.ronf.db.PagamentoDTO;

import java.util.Date;

public class NoleggioDTO implements Serializable {
	private long id;

	private AutoDTO auto;

	private ClienteDTO cliente;

	private Date inizio;

	private int durata;

	private AgenziaDTO agenziaPartenza;

	private AgenziaDTO agenziaArrivo;

	private String guidatoreAggiuntivo;

	private int seggiolini = 0;

	private boolean navigatore = false;

	public NoleggioDTO() { }

	public long getId() { return id; }
	public void setId(long id) { this.id = id; }

	public AutoDTO getAuto() { return auto; }
	public void setAuto(AutoDTO auto) { this.auto = auto; }

	public ClienteDTO getCliente() { return cliente; }
	public void setCliente(ClienteDTO cliente) { this.cliente = cliente; }

	public Date getInizio() { return inizio; }
	public void setInizio(Date inizio) { this.inizio = inizio; }

	public int getDurata() { return durata; }
	public void setDurata(int giorni) { this.durata = giorni; }

	public AgenziaDTO getAgenziaPartenza() { return agenziaPartenza; }
	public void setAgenziaPartenza(AgenziaDTO agenzia) {
		this.agenziaPartenza = agenzia;
	}

	public AgenziaDTO getAgenziaArrivo() { return agenziaArrivo; }
	public void setAgenziaArrivo(AgenziaDTO agenzia) {
		this.agenziaArrivo = agenzia;
	}

	public String getGuidatoreAggiuntivo() { return guidatoreAggiuntivo; }
	public void setGuidatoreAggiuntivo(String patente) {
		this.guidatoreAggiuntivo = patente;
	}

	public int getSeggiolini() { return seggiolini; }
	public void setSeggiolini(int seggiolini) {
		this.seggiolini = seggiolini;
	}

	public boolean getNavigatore() { return navigatore; }
	public void setNavigatore(boolean navigatore) {
		this.navigatore = navigatore;
	}
}
