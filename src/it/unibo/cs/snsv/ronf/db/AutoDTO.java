package it.unibo.cs.snsv.ronf.db;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

import it.unibo.cs.snsv.ronf.db.AgenziaDTO;

public class AutoDTO implements IsSerializable, Serializable {
	public enum Tipo implements IsSerializable {
		MINI,
		FAMILY,
		SPORT,
		PRESTIGE
	}

	private String targa;

	private String marca;

	private String modello;

	private Tipo tipo;

	private boolean disponibile = true;

	private AgenziaDTO agenzia;

	public AutoDTO() { }

	public Tipo getTipo() { return tipo; }
	public void setTipo(Tipo tipo) { this.tipo = tipo; }

	public String getTarga() { return targa; }
	public void setTarga(String targa) { this.targa = targa; }

	public String getMarca() { return marca; }
	public void setMarca(String marca) { this.marca = marca; }

	public String getModello() { return modello; }
	public void setModello(String modello) { this.modello = modello; }

	public boolean getDisponibile() { return disponibile; }
	public void setDisponibile(boolean disponibile) {
		this.disponibile = disponibile;
	}

	public AgenziaDTO getAgenzia() { return agenzia; }
	public void setAgenzia(AgenziaDTO agenzia) { this.agenzia = agenzia; }
}
