package it.unibo.cs.snsv.ronf.db;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;

import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO;

import org.dozer.DozerBeanMapperSingletonWrapper;

@Entity
@Table(name = "IMPIEGATI")
public class Impiegato extends Oggetto {
	public enum Ruolo {
		FRONT_OFFICE,
		MANUTENZIONE
	}

	@Id
	@GeneratedValue
	@Column
	private long id;

	@Column
	private String nome;

	@Column
	private String cognome;

	@Column
	@Enumerated(EnumType.STRING)
	private Ruolo ruolo;

	public Impiegato() { super(); }

	public long getId() { return id; }
	public void setId(long id) { this.id = id; }

	public Ruolo getRuolo() { return ruolo; }
	public void setRuolo(Ruolo ruolo) { this.ruolo = ruolo; }

	public String getNome() { return nome; }
	public void setNome(String nome) { this.nome = nome; }

	public String getCognome() { return cognome; }
	public void setCognome(String cognome) { this.cognome = cognome; }

	public static Impiegato trovaPerId(long id) {
		return Oggetto.trovaPerId(Impiegato.class, id);
	}

	public static Impiegato fromDTO(ImpiegatoDTO dto) {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(dto, Impiegato.class);
	}

	public ImpiegatoDTO toDTO() {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(this, ImpiegatoDTO.class);
	}
}
