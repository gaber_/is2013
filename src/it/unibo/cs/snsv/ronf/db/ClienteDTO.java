package it.unibo.cs.snsv.ronf.db;

import java.io.Serializable;

public class ClienteDTO implements Serializable {
	private String nome;

	private String cognome;

	private String patente;

	public ClienteDTO() { super(); }

	public String getPatente() { return patente; }
	public void setPatente(String patente) { this.patente = patente; }

	public String getNome() { return nome; }
	public void setNome(String nome) { this.nome = nome; }

	public String getCognome() { return cognome; }
	public void setCognome(String cognome) { this.cognome = cognome; }
}
