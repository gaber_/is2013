package it.unibo.cs.snsv.ronf.db;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

import org.hibernate.Session;
import org.hibernate.Transaction;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import it.unibo.cs.snsv.ronf.db.Agenzia;
import it.unibo.cs.snsv.ronf.db.Auto;
import it.unibo.cs.snsv.ronf.db.Impiegato;

import it.unibo.cs.snsv.ronf.db.TrasferimentoDTO;

import org.dozer.DozerBeanMapperSingletonWrapper;

import java.util.List;

@Entity
@Table(name = "TRASFERIMENTI")
public class Trasferimento extends Oggetto {
	@Id
	@GeneratedValue
	@Column
	private long id;

	@ManyToOne
	@JoinColumn
	private Auto auto;

	@ManyToOne
	@JoinColumn(nullable = true)
	private Impiegato addetto = null;

	@Column(name = "agenziaPartenza_id")
	private Agenzia agenziaPartenza;

	@Column(name = "agenziaArrivo_id")
	private Agenzia agenziaArrivo;

	@Column
	private boolean eseguito = false;

	public Trasferimento() { }

	public long getId() { return id; }
	public void setId(long id) { this.id = id; }

	public Auto getAuto() { return auto; }
	public void setAuto(Auto auto) { this.auto = auto; }

	public Impiegato getAddetto() { return addetto; }
	public void setAddetto(Impiegato addetto) { this.addetto = addetto; }

	public Agenzia getAgenziaPartenza() { return agenziaPartenza; }
	public void setAgenziaPartenza(Agenzia agenzia) {
		this.agenziaPartenza = agenzia;
	}

	public Agenzia getAgenziaArrivo() { return agenziaArrivo; }
	public void setAgenziaArrivo(Agenzia agenzia) {
		this.agenziaArrivo = agenzia;
	}

	public boolean getEseguito() { return eseguito; }
	public void setEseguito(boolean eseguito) { this.eseguito = eseguito; }

	public static Trasferimento trovaPerId(long id) {
		return Oggetto.trovaPerId(Trasferimento.class, id);
	}

	/**
	 * Trova tutti i trasferimenti non ancora eseguiti e senza addetto.
	 *
	 * @return lista di trasferimenti
	 */
	public static List<Trasferimento> trovaDaAssegnare() {
		List trasferimenti;

		Session session = HibernateUtil
			.getSessionFactory()
			.getCurrentSession();

		session.beginTransaction();
		trasferimenti = session.createCriteria(Trasferimento.class)
		       .add(Restrictions.and(
		         Restrictions.eq("eseguito", false),
		         Restrictions.isNull("addetto")
		       )).list();
		session.getTransaction().commit();

		return trasferimenti;
	}

	/**
	 * Trova i trasferimenti non eseguiti e assegnati a un dato addetto.
	 *
	 * @param addetto l'addetto assegnato
	 * @return        lista di trasferimenti
	 */
	public static List<Trasferimento> trovaPerAddetto(Impiegato addetto) {
		List trasferimenti;

		Session session = HibernateUtil
			.getSessionFactory()
			.getCurrentSession();

		session.beginTransaction();
		trasferimenti = session.createCriteria(Trasferimento.class)
		       .add(Restrictions.and(
		         Restrictions.eq("eseguito", false),
		         Restrictions.eq("addetto", addetto)
		       )).list();
		session.getTransaction().commit();

		return trasferimenti;
	}

	public static Trasferimento fromDTO(TrasferimentoDTO dto) {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(dto, Trasferimento.class);
	}

	public TrasferimentoDTO toDTO() {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(this, TrasferimentoDTO.class);
	}
}
