package it.unibo.cs.snsv.ronf.db;

import javax.persistence.MappedSuperclass;

import org.hibernate.Session;
import org.hibernate.Transaction;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import it.unibo.cs.snsv.ronf.db.HibernateUtil;

@MappedSuperclass
public class Oggetto {
	/**
	 * Crea un nuovo record nel DB corrispondente all'oggetto.
	 */
	public void crea() {
		Session session = HibernateUtil
			.getSessionFactory()
			.getCurrentSession();

		session.beginTransaction();
		session.save(this);
		session.getTransaction().commit();
	}

	/**
	 * Aggiorna il record del DB corrispondente all'oggetto.
	 */
	public void aggiorna() {
		Session session = HibernateUtil
			.getSessionFactory()
			.getCurrentSession();

		session.beginTransaction();
		session.update(this);
		session.getTransaction().commit();
	}

	/**
	 * Trova un oggetto nel DB dato un ID
	 *
	 * @param  class la classe dell'oggetto da trovare
	 * @param  id    l'ID dell'oggetto da trovare
	 * @return       l'oggetto trovato o null
	 */
	public static <T> T trovaPerId(Class <T> classe, long id) {
		T ogg;

		Session session = HibernateUtil
			.getSessionFactory()
			.getCurrentSession();

		session.beginTransaction();
		ogg = (T)session.createCriteria(classe)
		       .add(Restrictions.eq("id", id))
		       .uniqueResult();
		session.getTransaction().commit();

		return ogg;
	}
}
