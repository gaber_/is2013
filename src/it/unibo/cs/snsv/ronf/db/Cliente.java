package it.unibo.cs.snsv.ronf.db;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

import org.hibernate.Session;
import org.hibernate.Transaction;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import it.unibo.cs.snsv.ronf.db.ClienteDTO;

import org.dozer.DozerBeanMapperSingletonWrapper;

@Entity
@Table(name = "CLIENTI")
public class Cliente extends Oggetto {
	@Column
	private String nome;

	@Column
	private String cognome;

	@Id
	@Column
	private String patente;

	public Cliente() { super(); }

	public String getPatente() { return patente; }
	public void setPatente(String patente) { this.patente = patente; }

	public String getNome() { return nome; }
	public void setNome(String nome) { this.nome = nome; }

	public String getCognome() { return cognome; }
	public void setCognome(String cognome) { this.cognome = cognome; }

	public static Cliente trovaPerPatente(String patente) {
		Cliente cliente;

		Session session = HibernateUtil
			.getSessionFactory()
			.getCurrentSession();

		session.beginTransaction();
		cliente = (Cliente) session.createCriteria(Cliente.class)
		       .add(Restrictions.eq("patente", patente))
		       .uniqueResult();
		session.getTransaction().commit();

		return cliente;
	}

	public static Cliente fromDTO(ClienteDTO dto) {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(dto, Cliente.class);
	}

	public ClienteDTO toDTO() {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(this, ClienteDTO.class);
	}
}
