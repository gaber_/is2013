package it.unibo.cs.snsv.ronf.db;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.Type;

import org.hibernate.Session;
import org.hibernate.Transaction;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import it.unibo.cs.snsv.ronf.db.Agenzia;
import it.unibo.cs.snsv.ronf.db.Auto;
import it.unibo.cs.snsv.ronf.db.Auto.Tipo.*;
import it.unibo.cs.snsv.ronf.db.Cliente;
import it.unibo.cs.snsv.ronf.db.Pagamento;

import it.unibo.cs.snsv.ronf.server.ConfigUtil;

import it.unibo.cs.snsv.ronf.db.NoleggioDTO;

import org.dozer.DozerBeanMapperSingletonWrapper;

import java.util.Date;
import java.util.Calendar;
import java.util.List;

import java.lang.NullPointerException;

@Entity
@Table(name = "NOLEGGI")
public class Noleggio extends Oggetto {
	@Id
	@GeneratedValue
	@Column
	private long id;

	@ManyToOne
	@JoinColumn
	private Auto auto;

	@ManyToOne
	@JoinColumn
	private Cliente cliente;

	@Column
	@Type(type = "date")
	private Date inizio;

	@Column
	private int durata;

	@Column(name = "agenziaPartenza_id")
	private Agenzia agenziaPartenza;

	@Column(name = "agenziaArrivo_id")
	private Agenzia agenziaArrivo;

	@Column
	private String guidatoreAggiuntivo;

	@Column
	private int seggiolini = 0;

	@Column
	private boolean navigatore = false;

	public Noleggio() { }

	public long getId() { return id; }
	public void setId(long id) { this.id = id; }

	public Auto getAuto() { return auto; }
	public void setAuto(Auto auto) { this.auto = auto; }

	public Cliente getCliente() { return cliente; }
	public void setCliente(Cliente cliente) { this.cliente = cliente; }

	public Date getInizio() { return inizio; }
	public void setInizio(Date inizio) { this.inizio = inizio; }

	public int getDurata() { return durata; }
	public void setDurata(int giorni) { this.durata = giorni; }

	public Agenzia getAgenziaPartenza() { return agenziaPartenza; }
	public void setAgenziaPartenza(Agenzia agenzia) {
		this.agenziaPartenza = agenzia;
	}

	public Agenzia getAgenziaArrivo() { return agenziaArrivo; }
	public void setAgenziaArrivo(Agenzia agenzia) {
		this.agenziaArrivo = agenzia;
	}

	public String getGuidatoreAggiuntivo() { return guidatoreAggiuntivo; }
	public void setGuidatoreAggiuntivo(String patente) {
		this.guidatoreAggiuntivo = patente;
	}

	public int getSeggiolini() { return seggiolini; }
	public void setSeggiolini(int seggiolini) {
		this.seggiolini = seggiolini;
	}

	public boolean getNavigatore() { return navigatore; }
	public void setNavigatore(boolean navigatore) {
		this.navigatore = navigatore;
	}

	public static Noleggio trovaPerId(long id) {
		return Oggetto.trovaPerId(Noleggio.class, id);
	}

	/**
	 * Trova tutti i noleggi di una data auto.
	 *
	 * @param auto     l'auto
	 * @param daPagare se true, ritorna solo i noleggi non pagati
	 * @return         lista di noleggi
	 */
	public static List<Noleggio> trovaPerAuto(Auto auto, boolean daPagare) {
		List noleggi;

		Session session = HibernateUtil
			.getSessionFactory()
			.getCurrentSession();

		if (daPagare == false) {
			session.beginTransaction();
			noleggi = session.createCriteria(Noleggio.class)
				.add(Restrictions.eq("auto", auto))
				.list();
			session.getTransaction().commit();
		} else {
			session.beginTransaction();
			noleggi = session.createCriteria(Noleggio.class)
				.add(Restrictions.eq("auto", auto))
				.add(Restrictions.isNull("agenziaArrivo"))
				.list();
			session.getTransaction().commit();
		}

		return noleggi;
	}

	/**
	 * Determina se un noleggio è in corso.
	 *
	 * @return "true" se il noleggio è in corso
	 */
	public boolean inCorso() {
		Calendar cal = Calendar.getInstance();

		Date oggi = cal.getTime();

		cal.setTime(this.getInizio());
		cal.add(Calendar.DATE, this.getDurata());

		return cal.getTime().after(oggi);
	}

	/**
	 * Calcola il pagamento di un noleggio.
	 *
	 * L'oggetto {@link Pagamento} ritornato deve essere aggiunto al DB
	 * dall'utente con il metodo {@link #crea()}.
	 *
	 * @param penale aggiungi la penale per il mancato rifornimento
	 * @return       oggetto {@link Pagamento}
	 */
	public Pagamento calcolaPagamento(boolean penale) {
		double costo = 0.0;
		Pagamento pagamento = new Pagamento();

		String expr;
		String exprFmt = "//ronf-config/prezzi/prezzo[@id='%s']/text()";

		switch (this.getAuto().getTipo()) {
			case MINI:
				expr   = String.format(exprFmt, "mini");
				costo += ConfigUtil.getDouble(expr);
				break;
			case FAMILY:
				expr   = String.format(exprFmt, "family");
				costo += ConfigUtil.getDouble(expr);
				break;
			case SPORT:
				expr   = String.format(exprFmt, "sport");
				costo += ConfigUtil.getDouble(expr);
				break;
			case PRESTIGE:
				expr   = String.format(exprFmt, "prestige");
				costo += ConfigUtil.getDouble(expr);
				break;
		}

		costo *= getDurata();

		if (this.getGuidatoreAggiuntivo() != null &&
		    !this.getGuidatoreAggiuntivo().equals("")) {
			expr   = String.format(exprFmt, "guidatore");
			costo += ConfigUtil.getDouble(expr);
		}

		if (this.getSeggiolini() != 0) {
			expr   = String.format(exprFmt, "seggiolino");
			costo += ConfigUtil.getDouble(expr) * getSeggiolini();
		}

		if (this.getNavigatore() == true) {
			expr   = String.format(exprFmt, "navigatore");
			costo += ConfigUtil.getDouble(expr);
		}

		if (penale == true) {
			expr   = String.format(exprFmt, "penale");
			costo += ConfigUtil.getDouble(expr);
		}

		pagamento.setNoleggio(this);
		pagamento.setImporto(costo);

		return pagamento;
	}

	public static Noleggio fromDTO(NoleggioDTO dto) {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(dto, Noleggio.class);
	}

	public NoleggioDTO toDTO() {
		return DozerBeanMapperSingletonWrapper
			.getInstance()
			.map(this, NoleggioDTO.class);
	}
}
