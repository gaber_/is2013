package it.unibo.cs.snsv.ronf.client;

import it.unibo.cs.snsv.ronf.db.AutoDTO;
import it.unibo.cs.snsv.ronf.db.AutoDTO.Tipo.*;
import it.unibo.cs.snsv.ronf.db.ClienteDTO;
import it.unibo.cs.snsv.ronf.db.NoleggioDTO;
import it.unibo.cs.snsv.ronf.db.PagamentoDTO;
import it.unibo.cs.snsv.ronf.db.TrasferimentoDTO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;

@RemoteServiceRelativePath("frontoffice")
public interface FrontOffice extends RemoteService {
	ClienteDTO registraCliente(ClienteDTO cliente);

	ClienteDTO cercaCliente(String patente);

	AutoDTO registraAuto(AutoDTO auto);

	AutoDTO aggiornaAuto(AutoDTO input);

	AutoDTO cercaAuto(String targa);

	List<AutoDTO> cercaAutoPerTipo(AutoDTO.Tipo tipo);

	List<AutoDTO> cercaAutoLocaliPerTipo(AutoDTO.Tipo tipo);

	List<AutoDTO> cercaAutoRemotePerTipo(AutoDTO.Tipo tipo);

	NoleggioDTO registraNoleggio(NoleggioDTO input);

	NoleggioDTO aggiornaNoleggio(NoleggioDTO input);

	NoleggioDTO cercaNoleggioLocalePerAuto(AutoDTO input);

	NoleggioDTO cercaNoleggioRemotoPerAuto(AutoDTO input);

	PagamentoDTO calcolaPagamento(String targa);

	PagamentoDTO registraPagamento(PagamentoDTO input, boolean daRifornire);

	TrasferimentoDTO registraTrasferimento(TrasferimentoDTO input);
}
