package it.unibo.cs.snsv.ronf.client;

import it.unibo.cs.snsv.ronf.db.AutoDTO;
import it.unibo.cs.snsv.ronf.db.AutoDTO.Tipo.*;
import it.unibo.cs.snsv.ronf.db.ClienteDTO;
import it.unibo.cs.snsv.ronf.db.NoleggioDTO;
import it.unibo.cs.snsv.ronf.db.PagamentoDTO;
import it.unibo.cs.snsv.ronf.db.TrasferimentoDTO;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

public interface FrontOfficeAsync {
	void registraCliente(ClienteDTO cliente,
		AsyncCallback<ClienteDTO> callback);

	void cercaCliente(String patente,
		AsyncCallback<ClienteDTO> callback);

	void registraAuto(AutoDTO auto,
		AsyncCallback<AutoDTO> callback);

	void aggiornaAuto(AutoDTO auto,
		AsyncCallback<AutoDTO> callback);

	void cercaAuto(String targa,
		AsyncCallback<AutoDTO> callback);

	void cercaAutoPerTipo(AutoDTO.Tipo tipo,
		AsyncCallback<List<AutoDTO>> callback);

	void cercaAutoLocaliPerTipo(AutoDTO.Tipo tipo,
		AsyncCallback<List<AutoDTO>> callback);

	void cercaAutoRemotePerTipo(AutoDTO.Tipo tipo,
		AsyncCallback<List<AutoDTO>> callback);

	void registraNoleggio(NoleggioDTO input,
		AsyncCallback<NoleggioDTO> callback);

	void aggiornaNoleggio(NoleggioDTO input,
		AsyncCallback<NoleggioDTO> callback);

	void cercaNoleggioLocalePerAuto(AutoDTO input,
		AsyncCallback<NoleggioDTO> callback);

	void cercaNoleggioRemotoPerAuto(AutoDTO input,
		AsyncCallback<NoleggioDTO> callback);

	void calcolaPagamento(String targa,
		AsyncCallback<PagamentoDTO> callback);

	void registraPagamento(PagamentoDTO input, boolean daRifornire,
		AsyncCallback<PagamentoDTO> callback);

	void registraTrasferimento(TrasferimentoDTO input,
		AsyncCallback<TrasferimentoDTO> callback);
}
