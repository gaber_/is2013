package it.unibo.cs.snsv.ronf.client;

import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface LoginAsync {
	/**
	 * Crea un nuovo record impiegato in modo asincrono.
	 *
	 * @param  input    DTO rappresentante l'impiegato da aggiungere
	 * @param  callback callback per la chiamata asincrona
	 */
	void creaUtente(ImpiegatoDTO input,
		AsyncCallback<ImpiegatoDTO> callback);

	/**
	 * Autentica un impiegato in modo asincrono.
	 *
	 * @param  id       l'ID dell'impiegato da autenticare
	 * @param  callback callback per la chiamata asincrona
	 */
	void loginUtente(long id, AsyncCallback<ImpiegatoDTO> callback);
}
