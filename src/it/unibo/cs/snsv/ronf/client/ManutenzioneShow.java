package it.unibo.cs.snsv.ronf.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.Window;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;
import java.util.LinkedList;

import it.unibo.cs.snsv.ronf.db.RifornimentoDTO;
import it.unibo.cs.snsv.ronf.db.TrasferimentoDTO;
import it.unibo.cs.snsv.ronf.db.AutoDTO;
import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO;

public class ManutenzioneShow {
	static ImpiegatoDTO impiegato;

	public static void load(ImpiegatoDTO nimpiegato) {
		impiegato = nimpiegato;

		VerticalPanel mainPanel = new VerticalPanel();

		RootPanel rootPanel = RootPanel.get("RONF");
		rootPanel.clear();
		rootPanel.add(mainPanel);

		TabPanel tabPanel = new TabPanel();
		mainPanel.add(tabPanel);
		tabPanel.setSize("100%", "auto");

		FlexTable rifornimentiPanel = creaRifornimentiPanel(new FlexTable());
		tabPanel.add(rifornimentiPanel, "Gestisci Rifornimenti", false);

		FlexTable trasferimentiPanel = creaTrasferimentiPanel(new FlexTable());
		tabPanel.add(trasferimentiPanel, "Gestisci Trasferimenti", false);

		tabPanel.selectTab(0);
	}

	private static FlexTable creaRifornimentiPanel(final FlexTable table) {
		final ManutenzioneAsync manutenzione = GWT.create(Manutenzione.class);

		AsyncCallback<List<RifornimentoDTO>> callback = new AsyncCallback<List<RifornimentoDTO>>() {
			public void onFailure(Throwable caught) {
			}

			public void onSuccess(List<RifornimentoDTO> lista) {
				table.setText(0, 0, "ID      ");
				table.setText(0, 1, "Targa   ");
				table.setText(0, 2, "Tipo    ");
				table.setText(0, 3, "Addetto ");
				table.setText(0, 4, "        ");

				int i = 1;
				for (final RifornimentoDTO current : lista) {
					AutoDTO auto = current.getAuto();

					table.setText(i, 0, "" + current.getId());
					table.setText(i, 1, auto.getTarga());

					switch (auto.getTipo()) {
						case MINI:
							table.setText(i, 2, "Mini");
							break;

						case FAMILY:
							table.setText(i, 2, "Family");
							break;

						case SPORT:
							table.setText(i, 2, "Sport");
							break;

						case PRESTIGE:
							table.setText(i, 2, "Prestige");
							break;
					}

					final Button button = new Button("Completato");

					if (current.getAddetto() == null){
						button.setText("Accetta");
						table.setText(i, 3, "");
					} else {
						table.setText(i, 3, "" + current.getAddetto().getId());
					}

					table.setWidget(i, 4, button);
					button.addClickHandler(new ClickHandler() {
						public void onClick(ClickEvent event) {
							AsyncCallback<Void> doneCallback = new AsyncCallback<Void>() {
								public void onFailure(Throwable caught) {
								}

								public void onSuccess(Void result) {
									creaRifornimentiPanel(table);
								}
							};

							if (button.getText() == "Completato")
								manutenzione.rifornimentoEffettuato(current.getId(), doneCallback);
							else
								manutenzione.rifornimentoAssegnato(current.getId(), impiegato.getId(), doneCallback);
						}
					});

					i++;
				}

				while (i < table.getRowCount())
					table.removeRow(i);
			}
		};

		manutenzione.richiediRifornimenti(impiegato, callback);

		return table;
	}

	private static FlexTable creaTrasferimentiPanel(final FlexTable table) {
		final ManutenzioneAsync manutenzione = GWT.create(Manutenzione.class);

		AsyncCallback<List<TrasferimentoDTO>> callback = new AsyncCallback<List<TrasferimentoDTO>>() {
			public void onFailure(Throwable caught) {
			}

			public void onSuccess(List<TrasferimentoDTO> lista) {
				table.setText(0, 0, "ID      ");
				table.setText(0, 1, "Targa   ");
				table.setText(0, 2, "Tipo    ");
				table.setText(0, 3, "Addetto ");
				table.setText(0, 4, "        ");

				int i = 1;
				for (final TrasferimentoDTO current : lista) {
					AutoDTO auto = current.getAuto();

					table.setText(i, 0, "" + current.getId());
					table.setText(i, 1, auto.getTarga());

					switch (auto.getTipo()) {
						case MINI:
							table.setText(i, 2, "Mini");
							break;

						case FAMILY:
							table.setText(i, 2, "Family");
							break;

						case SPORT:
							table.setText(i, 2, "Sport");
							break;

						case PRESTIGE:
							table.setText(i, 2, "Prestige");
							break;
					}

					final Button button = new Button("Completato");

					if (current.getAddetto() == null){
						button.setText("Accetta");
						table.setText(i, 3, "");
					} else {
						table.setText(i, 3, "" + current.getAddetto().getId());
					}

					table.setWidget(i, 4, button);
					button.addClickHandler(new ClickHandler() {
						public void onClick(ClickEvent event) {
							AsyncCallback<Void> doneCallback = new AsyncCallback<Void>() {
								public void onFailure(Throwable caught) {
								}

								public void onSuccess(Void result) {
									creaTrasferimentiPanel(table);
								}
							};

							if (button.getText() == "Completato")
								manutenzione.trasferimentoEffettuato(current.getId(), doneCallback);
							else
								manutenzione.trasferimentoAssegnato(current.getId(), impiegato.getId(), doneCallback);
						}
					});

					i++;
				}

				while (i < table.getRowCount())
					table.removeRow(i);
			}
		};

		manutenzione.richiediTrasferimenti(impiegato, callback);

		return table;
	}
}
