package it.unibo.cs.snsv.ronf.client;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.Window;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO;

public class LoginShow {
	public static void load() {
		VerticalPanel mainPanel = new VerticalPanel();

		RootPanel rootPanel = RootPanel.get("RONF");
		rootPanel.clear();
		rootPanel.add(mainPanel);

		RootPanel userPanel = RootPanel.get("USER");
		userPanel.clear();;

		FlexTable loginPanel = new FlexTable();
		mainPanel.add(loginPanel);

		loginPanel.setText(0, 1, "Login");

		final TextBox idField = new TextBox();
		loginPanel.setText(1, 0, "User ID: ");
		loginPanel.setWidget(1, 1, idField);

		Button loginButton = new Button("Login");
		loginButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				AsyncCallback<ImpiegatoDTO> loginCallback = new AsyncCallback<ImpiegatoDTO>() {
					public void onSuccess(ImpiegatoDTO result) {
						if (result == null) {
							Window.alert("Impiegato non trovato.");
							return;
						}

						setUser(result);

						switch (result.getRuolo()) {
							case FRONT_OFFICE:
								FrontOfficeShow.load(result);
								break;

							case MANUTENZIONE:
								ManutenzioneShow.load(result);
								break;

							default:
								Window.alert("Blah");
								break;
						}
					}

					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}
				};

				LoginAsync login = (LoginAsync) GWT.create(Login.class);
				login.loginUtente(Long.parseLong(idField.getText()), loginCallback);
			}
		});

		loginPanel.setWidget(1, 2, loginButton);

		loginPanel.setText(2, 1, "Crea");

		final TextBox nomeField = new TextBox();
		loginPanel.setText(3, 0, "Nome: ");
		loginPanel.setWidget(3, 1, nomeField);

		final TextBox cognomeField = new TextBox();
		loginPanel.setText(4, 0, "Cognome: ");
		loginPanel.setWidget(4, 1, cognomeField);

		final ListBox ruoloField = new ListBox();
		ruoloField.addItem("Front-office");
		ruoloField.addItem("Manutenzione");
		loginPanel.setText(5, 0, "Ruolo: ");
		loginPanel.setWidget(5, 1, ruoloField);

		Button creaButton = new Button("New button");
		creaButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				AsyncCallback<ImpiegatoDTO> createCallback = new AsyncCallback<ImpiegatoDTO>() {
					public void onSuccess(ImpiegatoDTO result) {
						Window.alert("Creato utente con ID " + result.getId());
					}

					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}
				};

				LoginAsync login = (LoginAsync) GWT.create(Login.class);
				ImpiegatoDTO impiegato = new ImpiegatoDTO();

				impiegato.setNome(nomeField.getText());
				impiegato.setCognome(cognomeField.getText());

				impiegato.setRuolo(ImpiegatoDTO.Ruolo.values()[ruoloField.getSelectedIndex()]);

				login.creaUtente(impiegato, createCallback);
			}
		});
		creaButton.setText("Crea");
		loginPanel.setWidget(5, 2, creaButton);
	}

	public static void setUser(ImpiegatoDTO impiegato) {
		HorizontalPanel mainPanel = new HorizontalPanel();

		RootPanel userPanel = RootPanel.get("USER");
		userPanel.clear();
		userPanel.add(mainPanel);

		if (impiegato != null) {
			Label userLabel = new Label(
				impiegato.getNome() + " " +
				impiegato.getCognome() + " " +
				"(" + impiegato.getId() + ")"
			);

			mainPanel.add(userLabel);

			Anchor logoutAnchor = new Anchor("Logout");
			logoutAnchor.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					LoginShow.load();
				}

			});

			mainPanel.add(logoutAnchor);
		}
	}
}
