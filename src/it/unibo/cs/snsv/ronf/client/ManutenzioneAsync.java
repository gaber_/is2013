package it.unibo.cs.snsv.ronf.client;

import it.unibo.cs.snsv.ronf.db.RifornimentoDTO;
import it.unibo.cs.snsv.ronf.db.TrasferimentoDTO;

import it.unibo.cs.snsv.ronf.db.Impiegato;
import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;
import java.util.LinkedList;

public interface ManutenzioneAsync {
	void richiediRifornimenti(ImpiegatoDTO impiegato, AsyncCallback<List<RifornimentoDTO>> callback);
	void richiediTrasferimenti(ImpiegatoDTO impiegato, AsyncCallback<List<TrasferimentoDTO>> callback);
	void rifornimentoEffettuato(long idRifornimento, AsyncCallback<Void> callback);
	void rifornimentoAssegnato(long idRifornimento, long impiegato, AsyncCallback<Void> callback);
	void trasferimentoEffettuato(long idTrasferimento, AsyncCallback<Void> callback);
	void trasferimentoAssegnato(long idTrasferimento, long impiegato, AsyncCallback<Void> callback);
}
