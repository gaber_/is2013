package it.unibo.cs.snsv.ronf.client;

import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("login")
public interface Login extends RemoteService {
	/**
	 * Crea un nuovo record impiegato.
	 *
	 * @param  input DTO rappresentante l'impiegato da aggiungere
	 * @return       DTO rappresentante l'impiegato creato
	 */
	ImpiegatoDTO creaUtente(ImpiegatoDTO input);

	/**
	 * Autentica un impiegato.
	 *
	 * @param  id    l'ID dell'impiegato da autenticare
	 * @return       DTO rappresentante l'impiegato autenticato
	 */
	ImpiegatoDTO loginUtente(long id);
}
