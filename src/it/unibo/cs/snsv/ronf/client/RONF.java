package it.unibo.cs.snsv.ronf.client;

import com.google.gwt.core.client.EntryPoint;

public class RONF implements EntryPoint {
	public void onModuleLoad() {
		LoginShow.load();
	}
}
