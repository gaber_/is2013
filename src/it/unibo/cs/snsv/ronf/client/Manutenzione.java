package it.unibo.cs.snsv.ronf.client;

import it.unibo.cs.snsv.ronf.db.RifornimentoDTO;
import it.unibo.cs.snsv.ronf.db.TrasferimentoDTO;

import it.unibo.cs.snsv.ronf.db.Impiegato;
import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;

@RemoteServiceRelativePath("manutenzione")

public interface Manutenzione extends RemoteService {
	List<RifornimentoDTO> richiediRifornimenti(ImpiegatoDTO impiegato);
	List<TrasferimentoDTO> richiediTrasferimenti(ImpiegatoDTO impiegato);
	void rifornimentoEffettuato(long idRifornimento);
	void rifornimentoAssegnato(long idRifornimento, long impiegato);
	void trasferimentoEffettuato(long idTrasferimento);
	void trasferimentoAssegnato(long idTrasferimento, long impiegato);
}

