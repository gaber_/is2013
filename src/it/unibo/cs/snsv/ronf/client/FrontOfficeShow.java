package it.unibo.cs.snsv.ronf.client;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.SimpleCheckBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.Window;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import it.unibo.cs.snsv.ronf.db.AutoDTO;
import it.unibo.cs.snsv.ronf.db.ClienteDTO;
import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO;
import it.unibo.cs.snsv.ronf.db.NoleggioDTO;
import it.unibo.cs.snsv.ronf.db.PagamentoDTO;
import it.unibo.cs.snsv.ronf.db.TrasferimentoDTO;

import java.util.List;
import java.util.Date;

public class FrontOfficeShow {
	private static ClienteDTO cliente;

	public static void load(ImpiegatoDTO nimpiegato) {
		VerticalPanel mainPanel = new VerticalPanel();

		RootPanel rootPanel = RootPanel.get("RONF");
		rootPanel.clear();
		rootPanel.add(mainPanel);

		TabPanel tabPanel = new TabPanel();
		mainPanel.add(tabPanel);
		tabPanel.setSize("100%", "auto");

		FlexTable autoPanel = creaAutoPanel();
		tabPanel.add(autoPanel, "Registra Auto", false);

		FlexTable clientePanel = creaClientePanel();
		tabPanel.add(clientePanel, "Registra Cliente", false);

		FlexTable noleggioPanel = creaNoleggioPanel();
		tabPanel.add(noleggioPanel, "Registra Noleggio", false);

		FlexTable pagamentoPanel = creaPagamentoPanel();
		tabPanel.add(pagamentoPanel, "Registra Pagamento", false);

		FlexTable trasferimentoPanel = creaTrasferimentoPanel();
		tabPanel.add(trasferimentoPanel, "Registra Trasferimento", false);

		tabPanel.selectTab(0);
	}

	private static FlexTable creaAutoPanel() {
		FlexTable autoPanel = new FlexTable();

		final ListBox tipoField = new ListBox();
		tipoField.addItem("Mini");
		tipoField.addItem("Family");
		tipoField.addItem("Sport");
		tipoField.addItem("Prestige");
		autoPanel.setText(0, 0, "Tipo: ");
		autoPanel.setWidget(0, 1, tipoField);

		final TextBox targaField = new TextBox();
		autoPanel.setText(1, 0, "Targa: ");
		autoPanel.setWidget(1, 1, targaField);

		final TextBox marcaField = new TextBox();
		autoPanel.setText(2, 0, "Marca: ");
		autoPanel.setWidget(2, 1, marcaField);

		final TextBox modelloField = new TextBox();
		autoPanel.setText(3, 0, "Modello: ");
		autoPanel.setWidget(3, 1, modelloField);

		Button autoButton = new Button("Registra");
		autoButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				AsyncCallback<AutoDTO> creaCallback = new AsyncCallback<AutoDTO>() {
					public void onSuccess(AutoDTO result) {
						Window.alert("Registrata auto.");
					}

					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}
				};

				FrontOfficeAsync frontOffice = (FrontOfficeAsync) GWT.create(FrontOffice.class);
				AutoDTO auto = new AutoDTO();

				auto.setTipo(AutoDTO.Tipo.values()[tipoField.getSelectedIndex()]);
				auto.setTarga(targaField.getValue());
				auto.setMarca(marcaField.getValue());
				auto.setModello(modelloField.getValue());

				frontOffice.registraAuto(auto, creaCallback);
			}
		});
		autoPanel.setWidget(3, 2, autoButton);

		return autoPanel;
	}

	private static FlexTable creaClientePanel() {
		FlexTable clientePanel = new FlexTable();

		final TextBox nomeField = new TextBox();
		clientePanel.setText(0, 0, "Nome: ");
		clientePanel.setWidget(0, 1, nomeField);

		final TextBox cognomeField = new TextBox();
		clientePanel.setText(1, 0, "Cognome: ");
		clientePanel.setWidget(1, 1, cognomeField);

		final TextBox patenteField = new TextBox();
		clientePanel.setText(2, 0, "Patente: ");
		clientePanel.setWidget(2, 1, patenteField);

		Button clienteButton = new Button("Registra");
		clienteButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				AsyncCallback<ClienteDTO> creaCallback = new AsyncCallback<ClienteDTO>() {
					public void onSuccess(ClienteDTO result) {
						Window.alert("Registrato cliente.");
					}

					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}
				};

				FrontOfficeAsync frontOffice = (FrontOfficeAsync) GWT.create(FrontOffice.class);
				ClienteDTO cliente = new ClienteDTO();

				cliente.setNome(nomeField.getValue());
				cliente.setCognome(cognomeField.getValue());
				cliente.setPatente(patenteField.getValue());

				frontOffice.registraCliente(cliente, creaCallback);
			}
		});
		clientePanel.setWidget(2, 2, clienteButton);

		return clientePanel;
	}

	private static FlexTable creaNoleggioPanel() {
		final FlexTable noleggioPanel = new FlexTable();

		final TextBox patenteField = new TextBox();
		noleggioPanel.setText(0, 0, "Patente: ");
		noleggioPanel.setWidget(0, 1, patenteField);

		final Button cercaButton = new Button("Cerca");
		cercaButton.setEnabled(false);

		Button clienteButton = new Button("Cerca");
		clienteButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				AsyncCallback<ClienteDTO> clienteCallback = new AsyncCallback<ClienteDTO>() {
					public void onSuccess(ClienteDTO result) {
						if (result != null) {
							Window.alert("Cliente trovato.");

							cliente = result;

							cercaButton.setEnabled(true);
						} else {
							Window.alert("Cliente non trovato.");
							cercaButton.setEnabled(false);
						}
					}

					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}
				};

				FrontOfficeAsync frontOffice = (FrontOfficeAsync) GWT.create(FrontOffice.class);
				frontOffice.cercaCliente(patenteField.getValue(), clienteCallback);
			}
		});
		noleggioPanel.setWidget(0, 2, clienteButton);

		final TextBox durataField = new TextBox();
		durataField.setText("1");
		noleggioPanel.setText(1, 0, "Durata: ");
		noleggioPanel.setWidget(1, 1, durataField);

		final TextBox guidatoreAggiuntivoField = new TextBox();
		noleggioPanel.setText(2, 0, "Guidatore: ");
		noleggioPanel.setWidget(2, 1, guidatoreAggiuntivoField);

		final TextBox seggioliniField = new TextBox();
		seggioliniField.setText("0");
		noleggioPanel.setText(3, 0, "Seggiolini: ");
		noleggioPanel.setWidget(3, 1, seggioliniField);

		final SimpleCheckBox navigatoreField = new SimpleCheckBox();
		noleggioPanel.setText(4, 0, "Navigatore: ");
		noleggioPanel.setWidget(4, 1, navigatoreField);

		final ListBox tipoField = new ListBox();
		tipoField.addItem("Mini");
		tipoField.addItem("Family");
		tipoField.addItem("Sport");
		tipoField.addItem("Prestige");
		noleggioPanel.setText(5, 0, "Tipo auto: ");
		noleggioPanel.setWidget(5, 1, tipoField);

		cercaButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				final int durata;
				final int seggiolini;
				final String guidatoreAggiuntivo = guidatoreAggiuntivoField.getValue();
				final boolean navigatore = navigatoreField.getValue();

				try {
					durata = Integer.parseInt(durataField.getValue());
				} catch (NumberFormatException e) {
					Window.alert("Campo 'durata' invalido");
					return;
				}

				try {
					seggiolini = Integer.parseInt(seggioliniField.getValue());
				} catch (NumberFormatException e) {
					Window.alert("Campo 'seggiolini' invalido");
					return;
				}

				AsyncCallback<List<AutoDTO>> creaCallback = new AsyncCallback<List<AutoDTO>>() {
					public void onSuccess(List<AutoDTO> result) {
						final PopupPanel popupPanel = new PopupPanel(true);

						final FlexTable autoPanel = new FlexTable();
						popupPanel.add(autoPanel);

						if (result.size() > 0) {
							int i = 1;

							autoPanel.setText(0, 0, "Targa ");
							autoPanel.setText(0, 1, "Marca ");
							autoPanel.setText(0, 2, "Modello ");
							autoPanel.setText(0, 3, "Agenzia ");
							autoPanel.setText(0, 4, "Prenota ");

							for (AutoDTO auto : result) {
								autoPanel.setText(i, 0, auto.getTarga());
								autoPanel.setText(i, 1, auto.getMarca());
								autoPanel.setText(i, 2, auto.getModello());
								autoPanel.setText(i, 3, auto.getAgenzia().getNome());

								final NoleggioDTO noleggio = new NoleggioDTO();

								noleggio.setAuto(auto);
								noleggio.setCliente(cliente);
								noleggio.setInizio(new Date());
								noleggio.setDurata(durata);
								noleggio.setSeggiolini(seggiolini);
								noleggio.setGuidatoreAggiuntivo(guidatoreAggiuntivo);
								noleggio.setNavigatore(navigatore);

								Button prenotaButton = new Button("Prenota");
								prenotaButton.addClickHandler(new ClickHandler() {
									public void onClick(ClickEvent event) {
										AsyncCallback<NoleggioDTO> creaCallback = new AsyncCallback<NoleggioDTO>() {
											public void onSuccess(NoleggioDTO result) {
												Window.alert("Registrato noleggio con ID " + result.getId());
											}

											public void onFailure(Throwable caught) {
												Window.alert(caught.toString());
											}
										};

										FrontOfficeAsync frontOffice = (FrontOfficeAsync) GWT.create(FrontOffice.class);

										frontOffice.registraNoleggio(noleggio, creaCallback);

										popupPanel.hide();
									}
								});

								autoPanel.setWidget(i, 4, prenotaButton);

								i++;
							}


							popupPanel.center();
						} else {
							Window.alert("Nessuna auto trovata.");
						}
					}

					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}
				};

				FrontOfficeAsync frontOffice = (FrontOfficeAsync) GWT.create(FrontOffice.class);
				frontOffice.cercaAutoPerTipo(AutoDTO.Tipo.values()[tipoField.getSelectedIndex()], creaCallback);
			}
		});
		noleggioPanel.setWidget(5, 2, cercaButton);

		return noleggioPanel;
	}

	private static FlexTable creaPagamentoPanel() {
		FlexTable pagamentoPanel = new FlexTable();

		final TextBox idField = new TextBox();
		pagamentoPanel.setText(0, 0, "Auto: ");
		pagamentoPanel.setWidget(0, 1, idField);

		Button cercaButton = new Button("Cerca");
		cercaButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				AsyncCallback<PagamentoDTO> cercaCallback = new AsyncCallback<PagamentoDTO>() {
					public void onSuccess(PagamentoDTO result) {
						final PopupPanel popupPanel = new PopupPanel(true);

						final FlexTable noleggioPanel = new FlexTable();
						popupPanel.add(noleggioPanel);

						if (result != null) {
							final PagamentoDTO pagamento = result;

							noleggioPanel.setText(0, 0, "Importo: ");
							noleggioPanel.setText(0, 1, "" + result.getImporto());

							final ListBox metodoField = new ListBox();
							metodoField.addItem("Carta di Credito");
							metodoField.addItem("Contanti");
							noleggioPanel.setText(1, 0, "Metodo: ");
							noleggioPanel.setWidget(1, 1, metodoField);

							final SimpleCheckBox rifornireField = new SimpleCheckBox();
							noleggioPanel.setText(2, 0, "Da rifornire? ");
							noleggioPanel.setWidget(2, 1, rifornireField);

							Button pagaButton = new Button("Paga");
							pagaButton.addClickHandler(new ClickHandler() {
								public void onClick(ClickEvent event) {
									AsyncCallback<PagamentoDTO> creaCallback = new AsyncCallback<PagamentoDTO>() {
										public void onSuccess(PagamentoDTO result) {
											Window.alert("Registrato pagamento con ID " + result.getId());
										}

										public void onFailure(Throwable caught) {
											Window.alert(caught.toString());
										}
									};

									FrontOfficeAsync frontOffice = (FrontOfficeAsync) GWT.create(FrontOffice.class);

									pagamento.setMetodo(PagamentoDTO.Metodo.values()[metodoField.getSelectedIndex()]);

									frontOffice.registraPagamento(pagamento, rifornireField.getValue(), creaCallback);

									popupPanel.hide();
								}
							});

							noleggioPanel.setWidget(3, 2, pagaButton);

							popupPanel.center();
						} else {
							Window.alert("Nessun noleggio trovato.");
						}
					}

					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}
				};

				FrontOfficeAsync frontOffice = (FrontOfficeAsync) GWT.create(FrontOffice.class);
				frontOffice.calcolaPagamento(idField.getValue(), cercaCallback);
			}
		});
		pagamentoPanel.setWidget(0, 2, cercaButton);

		return pagamentoPanel;
	}

	private static FlexTable creaTrasferimentoPanel() {
		FlexTable trasferimentoPanel = new FlexTable();

		final ListBox tipoField = new ListBox();
		tipoField.addItem("Mini");
		tipoField.addItem("Family");
		tipoField.addItem("Sport");
		tipoField.addItem("Prestige");
		trasferimentoPanel.setText(0, 0, "Tipo auto: ");
		trasferimentoPanel.setWidget(0, 1, tipoField);

		Button cercaButton = new Button("Cerca");
		cercaButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				AsyncCallback<List<AutoDTO>> creaCallback = new AsyncCallback<List<AutoDTO>>() {
					public void onSuccess(List<AutoDTO> result) {
						final PopupPanel popupPanel = new PopupPanel(true);

						final FlexTable autoPanel = new FlexTable();
						popupPanel.add(autoPanel);

						if (result.size() > 0) {
							int i = 1;

							autoPanel.setText(0, 0, "Targa ");
							autoPanel.setText(0, 1, "Marca ");
							autoPanel.setText(0, 2, "Modello ");
							autoPanel.setText(0, 3, "Agenzia ");
							autoPanel.setText(0, 4, "Prenota ");

							for (AutoDTO auto : result) {
								autoPanel.setText(i, 0, auto.getTarga());
								autoPanel.setText(i, 1, auto.getMarca());
								autoPanel.setText(i, 2, auto.getModello());
								autoPanel.setText(i, 3, auto.getAgenzia().getNome());

								final TrasferimentoDTO trasferimento = new TrasferimentoDTO();

								trasferimento.setAuto(auto);

								Button prenotaButton = new Button("Trasferisci");
								prenotaButton.addClickHandler(new ClickHandler() {
									public void onClick(ClickEvent event) {
										AsyncCallback<TrasferimentoDTO> creaCallback = new AsyncCallback<TrasferimentoDTO>() {
											public void onSuccess(TrasferimentoDTO result) {
												Window.alert("Registrato trasferimento con ID " + result.getId());
											}

											public void onFailure(Throwable caught) {
												Window.alert(caught.toString());
											}
										};

										FrontOfficeAsync frontOffice = (FrontOfficeAsync) GWT.create(FrontOffice.class);

										frontOffice.registraTrasferimento(trasferimento, creaCallback);

										popupPanel.hide();
									}
								});

								autoPanel.setWidget(i, 4, prenotaButton);

								i++;
							}


							popupPanel.center();
						} else {
							Window.alert("Nessuna auto trovata.");
						}
					}

					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}
				};

				FrontOfficeAsync frontOffice = (FrontOfficeAsync) GWT.create(FrontOffice.class);
				frontOffice.cercaAutoRemotePerTipo(AutoDTO.Tipo.values()[tipoField.getSelectedIndex()], creaCallback);
			}
		});
		trasferimentoPanel.setWidget(1, 1, cercaButton);

		return trasferimentoPanel;
	}
}
