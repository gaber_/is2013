package it.unibo.cs.snsv.ronf.server;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class AppContext implements ServletContextListener{
	public void contextInitialized(ServletContextEvent contextEvent) {
		try {
			ConfigUtil.buildConfig();
		} catch (Exception e) {
		}
	}

	public void contextDestroyed(ServletContextEvent contextEvent) {
	}
}
