package it.unibo.cs.snsv.ronf.server;

import it.unibo.cs.snsv.ronf.client.Manutenzione;

import it.unibo.cs.snsv.ronf.db.Noleggio;

import it.unibo.cs.snsv.ronf.db.Rifornimento;
import it.unibo.cs.snsv.ronf.db.RifornimentoDTO;

import it.unibo.cs.snsv.ronf.db.Impiegato;
import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO;

import it.unibo.cs.snsv.ronf.db.Trasferimento;
import it.unibo.cs.snsv.ronf.db.TrasferimentoDTO;

import it.unibo.cs.snsv.ronf.db.Auto;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import org.dozer.DozerBeanMapperSingletonWrapper;

import java.util.List;
import java.util.LinkedList;

public class ManutenzioneImpl extends RemoteServiceServlet
					implements Manutenzione {
	@Override
	public List<RifornimentoDTO> richiediRifornimenti(ImpiegatoDTO impiegatoDTO) {
		List<RifornimentoDTO> output = new LinkedList();
		Impiegato impiegato = Impiegato.fromDTO(impiegatoDTO);

		List<Rifornimento> lista = Rifornimento.trovaDaAssegnare();
		List<Rifornimento> listaImpiegato = Rifornimento
			.trovaPerAddetto(impiegato);

		for (Rifornimento i : listaImpiegato)
			output.add(i.toDTO());

		for (Rifornimento i : lista) {
			output.add(i.toDTO());


			if (output.size() >= 10)
				break;
		}

		return output;
	}

	@Override
	public List<TrasferimentoDTO> richiediTrasferimenti(ImpiegatoDTO impiegatoDTO) {
		List<TrasferimentoDTO> output = new LinkedList();
		Impiegato impiegato = Impiegato.fromDTO(impiegatoDTO);

		List<Trasferimento> lista = Trasferimento.trovaDaAssegnare();
		List<Trasferimento> listaImpiegato = Trasferimento
			.trovaPerAddetto(impiegato);

		for (Trasferimento i : listaImpiegato)
			output.add(i.toDTO());

		for (Trasferimento i : lista) {
			output.add(i.toDTO());

			if (output.size() >= 1)
				break;
		}

		return output;
	}

	@Override
	public void rifornimentoEffettuato(long idRifornimento) {
		Rifornimento rifornimento = Rifornimento.trovaPerId(idRifornimento);
		Auto auto = rifornimento.getAuto();

		rifornimento.setEseguito(true);
		rifornimento.aggiorna();

		auto.setDisponibile(true);
		auto.aggiorna();

		return;
	}

	@Override
	public void rifornimentoAssegnato(long idRifornimento, long idImpiegato) {
		Impiegato impiegato = Impiegato.trovaPerId(idImpiegato);
		Rifornimento rifornimento = Rifornimento.trovaPerId(idRifornimento);

		rifornimento.setAddetto(impiegato);
		rifornimento.aggiorna();
		return;
	}

	@Override
	public void trasferimentoEffettuato(long idTrasferimento) {
		Trasferimento trasferimento = Trasferimento.trovaPerId(idTrasferimento);
		Auto auto = trasferimento.getAuto();

		trasferimento.setEseguito(true);
		trasferimento.aggiorna();

		if (Noleggio.trovaPerAuto(auto, true).size() == 0) {
			auto.setDisponibile(true);
			auto.aggiorna();
		}

		return;
	}

	@Override
	public void trasferimentoAssegnato(long idTrasferimento, long idImpiegato) {
		Impiegato impiegato = Impiegato.trovaPerId(idImpiegato);
		Trasferimento trasferimento = Trasferimento.trovaPerId(idTrasferimento);

		trasferimento.setAddetto(impiegato);
		trasferimento.aggiorna();

		return;
	}
}
