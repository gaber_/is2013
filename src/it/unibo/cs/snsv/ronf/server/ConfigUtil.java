package it.unibo.cs.snsv.ronf.server;

import java.io.InputStream;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.xpath.*;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathConstants.*;

public class ConfigUtil {
	private static Document config;
	private static XPathFactory xPathFactory;

	public static void buildConfig() throws
				javax.xml.parsers.ParserConfigurationException,
				org.xml.sax.SAXException, java.io.IOException {

		InputStream input = ConfigUtil.class.getClassLoader()
			.getResourceAsStream("RONF.cfg.xml");

		DocumentBuilderFactory docBuilderFactory =
			DocumentBuilderFactory.newInstance();

		DocumentBuilder docBuilder =
			docBuilderFactory.newDocumentBuilder();

		config = docBuilder.parse(input);
		xPathFactory = XPathFactory.newInstance();
	}

	public static String getString(String query) {
		try {
			XPath xpath = xPathFactory.newXPath();
			XPathExpression expr = xpath.compile(query);

			return (String) expr.evaluate(config, XPathConstants.STRING);
		} catch (Exception e) {
			return null;
		}
	}

	public static double getDouble(String query) {
		String str = getString(query);
		return Double.parseDouble(str);
	}

	public static long getLong(String query) {
		String str = getString(query);
		return Long.parseLong(str);
	}

	public static NodeList getNodeList(String query) {
		try {
			XPath xpath = xPathFactory.newXPath();
			XPathExpression expr = xpath.compile(query);

			return (NodeList) expr.evaluate(config,
					XPathConstants.NODESET);
		} catch (Exception e) {
			return null;
		}
	}
}
