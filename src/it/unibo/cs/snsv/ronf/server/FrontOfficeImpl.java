package it.unibo.cs.snsv.ronf.server;

import it.unibo.cs.snsv.ronf.client.FrontOffice;

import it.unibo.cs.snsv.ronf.db.Agenzia;
import it.unibo.cs.snsv.ronf.db.AgenziaDTO;

import it.unibo.cs.snsv.ronf.db.Auto;
import it.unibo.cs.snsv.ronf.db.AutoDTO;

import it.unibo.cs.snsv.ronf.db.Cliente;
import it.unibo.cs.snsv.ronf.db.ClienteDTO;

import it.unibo.cs.snsv.ronf.db.Noleggio;
import it.unibo.cs.snsv.ronf.db.NoleggioDTO;

import it.unibo.cs.snsv.ronf.db.Pagamento;
import it.unibo.cs.snsv.ronf.db.PagamentoDTO;

import it.unibo.cs.snsv.ronf.db.Rifornimento;

import it.unibo.cs.snsv.ronf.db.Trasferimento;
import it.unibo.cs.snsv.ronf.db.TrasferimentoDTO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import com.gdevelop.gwt.syncrpc.SyncProxy;

import org.dozer.DozerBeanMapperSingletonWrapper;

import java.util.List;
import java.util.LinkedList;

public class FrontOfficeImpl extends RemoteServiceServlet
					implements FrontOffice {
	@Override
	public ClienteDTO registraCliente(ClienteDTO input) {
		Cliente cliente = Cliente.fromDTO(input);

		cliente.crea();

		return cliente.toDTO();
	}

	@Override
	public ClienteDTO cercaCliente(String patente) {
		Cliente cliente = Cliente.trovaPerPatente(patente);

		if (cliente == null)
			return null;

		return cliente.toDTO();
	}

	@Override
	public AutoDTO registraAuto(AutoDTO input) {
		Auto auto = Auto.fromDTO(input);
		Agenzia agenzia = Agenzia.trovaLocale();

		auto.setAgenzia(agenzia);

		auto.crea();

		return auto.toDTO();
	}

	@Override
	public AutoDTO aggiornaAuto(AutoDTO input) {
		Auto auto = Auto.fromDTO(input);

		auto.aggiorna();

		return auto.toDTO();
	}

	@Override
	public AutoDTO cercaAuto(String targa) {
		Auto auto = Auto.trovaPerTarga(targa);

		if (auto == null)
			return null;

		return auto.toDTO();
	}

	@Override
	public List<AutoDTO> cercaAutoPerTipo(AutoDTO.Tipo input) {
		List<AutoDTO> output = cercaAutoLocaliPerTipo(input);

		if (output.size() == 0)
			return cercaAutoRemotePerTipo(input);

		return output;
	}

	@Override
	public List<AutoDTO> cercaAutoLocaliPerTipo(AutoDTO.Tipo input) {
		Auto.Tipo tipo;

		List<Auto> auto;
		List<AutoDTO> output = new LinkedList();

		switch (input) {
			case MINI: tipo = Auto.Tipo.MINI; break;
			default: case FAMILY: tipo = Auto.Tipo.FAMILY; break;
			case SPORT: tipo = Auto.Tipo.SPORT; break;
			case PRESTIGE: tipo = Auto.Tipo.PRESTIGE; break;
		}

		auto = Auto.trovaPerTipo(tipo, true);

		for (Auto i : auto)
			output.add(i.toDTO());

		return output;
	}

	@Override
	public List<AutoDTO> cercaAutoRemotePerTipo(AutoDTO.Tipo input) {
		Auto.Tipo tipo;

		List<Auto> auto;
		List<AutoDTO> output = new LinkedList();

		switch (input) {
			case MINI: tipo = Auto.Tipo.MINI; break;
			default: case FAMILY: tipo = Auto.Tipo.FAMILY; break;
			case SPORT: tipo = Auto.Tipo.SPORT; break;
			case PRESTIGE: tipo = Auto.Tipo.PRESTIGE; break;
		}

		List<Agenzia> agenzie = Agenzia.trovaRemote();

		for (Agenzia agenzia : agenzie) {
			FrontOffice frontoffice =
				(FrontOffice) SyncProxy
				.newProxyInstance(FrontOffice.class,
						agenzia.getUrl(), "frontoffice");

			output.addAll(0, frontoffice.cercaAutoLocaliPerTipo(input));
		}

		return output;
	}

	@Override
	public NoleggioDTO registraNoleggio(NoleggioDTO input) {
		Noleggio noleggio = Noleggio.fromDTO(input);
		Auto     auto     = noleggio.getAuto();
		Agenzia  locale   = Agenzia.trovaLocale();

		if (auto.getAgenzia().getId() != locale.getId()) {
			TrasferimentoDTO trasferimento = new TrasferimentoDTO();

			trasferimento.setAuto(input.getAuto());

			registraTrasferimento(trasferimento);

			auto = Auto.trovaPerTarga(auto.getTarga());
		}

		noleggio.setAgenziaPartenza(Agenzia.trovaLocale());
		noleggio.setAgenziaArrivo(null);

		auto.setDisponibile(false);
		auto.aggiorna();

		noleggio.crea();

		return noleggio.toDTO();
	}

	@Override
	public NoleggioDTO aggiornaNoleggio(NoleggioDTO input) {
		Noleggio noleggio = Noleggio.fromDTO(input);

		noleggio.aggiorna();

		return noleggio.toDTO();
	}

	@Override
	public NoleggioDTO cercaNoleggioLocalePerAuto(AutoDTO input) {
		Auto auto = Auto.fromDTO(input);
		List<Noleggio> noleggi = Noleggio.trovaPerAuto(auto, true);

		if (noleggi.size() == 0)
			return null;

		return noleggi.get(0).toDTO();
	}

	@Override
	public NoleggioDTO cercaNoleggioRemotoPerAuto(AutoDTO input) {
		NoleggioDTO   noleggio = null;
		List<Agenzia> agenzie = Agenzia.trovaRemote();

		for (Agenzia agenzia : agenzie) {
			FrontOffice frontoffice =
				(FrontOffice) SyncProxy
				.newProxyInstance(FrontOffice.class,
						agenzia.getUrl(), "frontoffice");

			noleggio = frontoffice.cercaNoleggioLocalePerAuto(input);

			if (noleggio != null)
				return noleggio;
		}

		return null;
	}

	@Override
	public PagamentoDTO calcolaPagamento(String targa) {
		AutoDTO auto = new AutoDTO();
		auto.setTarga(targa);

		NoleggioDTO dto = cercaNoleggioLocalePerAuto(auto);

		if (dto == null)
			dto = cercaNoleggioRemotoPerAuto(auto);

		if (dto == null)
			return null;

		Noleggio noleggio = Noleggio.fromDTO(dto);
		return noleggio.calcolaPagamento(noleggio.inCorso()).toDTO();
	}

	@Override
	public PagamentoDTO registraPagamento(PagamentoDTO input, boolean daRifornire) {
		Pagamento pagamento = Pagamento.fromDTO(input);
		Noleggio noleggio   = pagamento.getNoleggio();

		Agenzia locale = Agenzia.trovaLocale();

		Auto auto = noleggio.getAuto();

		noleggio.setAgenziaArrivo(locale);

		if (auto.getAgenzia().getId() != locale.getId()) {
			Cliente cliente = noleggio.getCliente();

			auto.setAgenzia(locale);

			FrontOffice frontoffice = (FrontOffice) SyncProxy
				.newProxyInstance(FrontOffice.class,
				noleggio.getAgenziaPartenza().getUrl(),
				"frontoffice");

			frontoffice.aggiornaNoleggio(noleggio.toDTO());

			if (Auto.trovaPerTarga(auto.getTarga()) != null)
				auto.aggiorna();
			else
				auto.crea();

			if (Cliente.trovaPerPatente(cliente.getPatente()) == null)
				cliente.crea();

			noleggio.crea();
		}

		noleggio.aggiorna();

		pagamento.crea();

		if (daRifornire) {
			Rifornimento rifornimento = new Rifornimento();
			rifornimento.setAuto(auto);

			rifornimento.crea();
		} else {
			auto.setDisponibile(true);
			auto.aggiorna();
		}

		return pagamento.toDTO();
	}

	@Override
	public TrasferimentoDTO registraTrasferimento(TrasferimentoDTO input) {
		Auto auto = Auto.fromDTO(input.getAuto());

		Agenzia locale = Agenzia.trovaLocale();
		Agenzia remota = Agenzia.fromDTO(input.getAuto().getAgenzia());

		Trasferimento trasferimento = Trasferimento.fromDTO(input);

		trasferimento.setAgenziaPartenza(remota);
		trasferimento.setAgenziaArrivo(locale);

		auto.setDisponibile(false);
		auto.setAgenzia(locale);

		if (Auto.trovaPerTarga(auto.getTarga()) != null)
			auto.aggiorna();
		else
			auto.crea();

		trasferimento.crea();

		FrontOffice frontoffice = (FrontOffice) SyncProxy
				.newProxyInstance(FrontOffice.class,
				remota.getUrl(), "frontoffice");

		frontoffice.aggiornaAuto(auto.toDTO());

		return trasferimento.toDTO();
	}
}
