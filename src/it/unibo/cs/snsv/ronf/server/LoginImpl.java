package it.unibo.cs.snsv.ronf.server;

import it.unibo.cs.snsv.ronf.client.Login;

import it.unibo.cs.snsv.ronf.db.Impiegato;
import it.unibo.cs.snsv.ronf.db.ImpiegatoDTO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class LoginImpl extends RemoteServiceServlet implements Login {
	@Override
	public ImpiegatoDTO creaUtente(ImpiegatoDTO input) {
		Impiegato impiegato = Impiegato.fromDTO(input);

		impiegato.crea();

		return impiegato.toDTO();
	}

	@Override
	public ImpiegatoDTO loginUtente(long id) {
		Impiegato impiegato = Impiegato.trovaPerId(id);

		if (impiegato == null)
			return null;

		return impiegato.toDTO();
	}
}
